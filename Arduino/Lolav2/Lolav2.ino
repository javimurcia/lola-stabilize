#include "lola_board.h"


#define pulsosgradosgrande 15.8

#define VEL_DER_REF_170 185  // Valor de la PWM
#define DIST_RUEDAS     194  // mm
float K =0.2083;             // Constante mm/pulso

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~VARIABLES GLOBALES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
static uint8_t velIZQ, velDER;
uint8_t i = 0;
char inChar;
char orden[1];
volatile unsigned int encoderIZQ = 0, encoderDER = 0, encoderLIM = 0;
uint16_t valor = 0;
char auxencoder = '0';
char auxencoder_avanzar = '0';
char auxvelocidad = '0';
char auxvelocidad_avanzar = '0';
char auxbloqueo = '0';
char auxatrasizq = '0';
char auxatrasder = '0';
uint8_t velservo = 30;
uint8_t poservo = 0;
unsigned int limitencoder = 0;
unsigned int limitencoder_avanzar = 0;
uint16_t grados = 0;
uint8_t enc = 0;
float pulsosgrados = 9.5;
char sensores[7];
char encoderizq[20];
char encoderder[7];
unsigned long l_pulses = 0;
unsigned long r_pulses = 0;
int L = 0;
float s = 0;
volatile float D = 0;
const float pi = 3.141592653;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~PID~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

int preverror=0;         //left motor previous error
int Ierror=0;            //left motor intergral error        
int error;        //error from right encoder
int derror;       //derivative error left
float control;      // control a aplicar a la velocidad derecha
/////////////////////////////////////////////
/////////////////////////////////////////////
// CONSTANTES A AJUSTAR
/////////////////////////////////////////////
/////////////////////////////////////////////
float KP = 0.1;       //PID proportional gain constant
float KD = 0.25;     //PID derivative gain constant
float KI = 0;//.125;   //PID intergral gain constant
int distancia;



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DECL. FUNCIONES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void analizar_orden(void);
short int leer_numero(void);
void hw_init();
void cuentaDER()
{
  encoderDER++;    //Cada vez que detecta un pulso sube una cuenta.
  //Serial3.println("encoder derecho")
  //Serial3.println(encoderDER)
  
}

void cuentaIZQ()
{
  encoderIZQ++;  //Cada vez que detecta un pulso sube una cuenta.
  //Serial3.println("encoder izquierdo")
  //Serial3.println(encoderIZQ)
  
}


void setup() {
  hw_init();

  //begin testing
  //print_message();

  attachInterrupt(digitalPinToInterrupt(MOT_R_ENC_B_PIN), cuentaDER, FALLING);         //Configura las int. ext. por flanco de bajada.
  attachInterrupt(digitalPinToInterrupt(MOT_L_ENC_B_PIN), cuentaIZQ, FALLING);

  delay(2500);
  
  analogWrite(MOT_R_PWM_PIN, 0);                                                   //Colocamos los motores a 0 por precaucion.
  analogWrite(MOT_L_PWM_PIN, 0);

  digitalWrite(MOT_R_B_PIN, LOW);
  digitalWrite(MOT_L_B_PIN, LOW);
  digitalWrite(MOT_R_A_PIN, LOW);
  digitalWrite(MOT_L_A_PIN, LOW);

   Serial3.begin(9600);
   
}

void loop() { 
 if (Serial3.available() > 0)                                           //Si hay algo disponible en el puerto serie lo lee y lo
  { 
    Serial3.readBytes(orden, 1);
    analizar_orden();     //Llama a la funcion "analizar_cadena".
  }
    if(auxencoder == '1')
     {
        if(encoderDER>=(limitencoder-100) && auxvelocidad == '1')
        {
           analogWrite(MOT_R_PWM_PIN, 80);
           auxvelocidad='0';
        }
        if(encoderDER>=limitencoder)
        {
          digitalWrite(MOT_R_B_PIN, HIGH);
          digitalWrite(MOT_R_A_PIN, HIGH);
          analogWrite(MOT_R_PWM_PIN, 255);
          digitalWrite(MOT_L_A_PIN, HIGH);
          digitalWrite(MOT_L_B_PIN, HIGH);
          analogWrite(MOT_L_PWM_PIN,255);
          limitencoder = 0;
          auxencoder = 0;  
        
        }
    }

    if (auxencoder_avanzar =='1')
    {
      if (encoderIZQ<limitencoder_avanzar)
        {
          error = encoderIZQ-encoderDER;  //calculate error values
          derror = (error - preverror);         // derivative error
          control = ((KP*error) + (KD*derror) + (KI*Ierror)); //PID equations
          if((velDER + control) > 255) //prevent OCR0 and OCR2 from overrunning 255
            velDER = 255;
          else if ((velDER+control)<0 )
              velDER=0;
          else
             {velDER = velDER + control; 
             //Serial3.println("Ajustamos velDER");
             }    //use output from PID equations to alter motor speeds
          
          preverror = error;          //set previous error to current error
          Ierror = Ierror + error;    //add current error to integral error
          
          analogWrite(MOT_R_PWM_PIN, velDER);
          delay(250);
          
        }
        else {
          //Paramos los motores
          PORTA = PORTA | (B1010101);
          analogWrite(MOT_L_PWM_PIN, 255);
          analogWrite(MOT_R_PWM_PIN,255);
          
          limitencoder_avanzar = 0;
         
          auxencoder_avanzar = 0;
        }
     }
  
}//END LOOP


void hw_init(){
//Battery pin for voltaje measurement
  pinMode(BAT_PIN,         INPUT);

//Dip switch for configuration
  pinMode(SW1_PIN,  INPUT_PULLUP);
  pinMode(SW2_PIN,  INPUT_PULLUP);
  pinMode(SW3_PIN,  INPUT_PULLUP);
  pinMode(SW4_PIN,  INPUT_PULLUP);
  pinMode(SW5_PIN,  INPUT_PULLUP);
  pinMode(SW6_PIN,  INPUT_PULLUP);
  pinMode(SW7_PIN,  INPUT_PULLUP);
  pinMode(SW8_PIN,  INPUT_PULLUP);

//Rear electronics box
  pinMode(REAR_LED_PIN,   OUTPUT);
  pinMode(REAR_SW_PIN, INPUT_PULLUP);

//Buzzer
  pinMode(BUZZER_PIN,     OUTPUT);

//L Motor
  pinMode(MOT_L_PWM_PIN,  OUTPUT);
  pinMode(MOT_L_A_PIN,    OUTPUT);
  pinMode(MOT_L_B_PIN,    OUTPUT);
  pinMode(MOT_L_ENC_A_PIN, INPUT);
  pinMode(MOT_L_ENC_B_PIN, INPUT);

//R Motor
  pinMode(MOT_R_PWM_PIN,  OUTPUT);
  pinMode(MOT_R_A_PIN,    OUTPUT);
  pinMode(MOT_R_B_PIN,    OUTPUT);
  pinMode(MOT_R_ENC_A_PIN, INPUT);
  pinMode(MOT_R_ENC_B_PIN, INPUT);

//L RGB LED
  pinMode(L_RED_PIN,      OUTPUT);
  pinMode(L_GRE_PIN,      OUTPUT);
  pinMode(L_BLU_PIN,      OUTPUT);
  
//R RGB LED
  pinMode(R_RED_PIN,      OUTPUT);
  pinMode(R_GRE_PIN,      OUTPUT);
  pinMode(R_BLU_PIN,      OUTPUT);

/*//AUX RGB LED
  pinMode(AUX_RED_PIN,    OUTPUT);
  pinMode(AUX_GRE_PIN,    OUTPUT);
  pinMode(AUX_BLU_PIN,    OUTPUT);*/

//F FALL/BUMPER SENSOR
  pinMode(F_FALL_PIN,      INPUT);
  pinMode(F_BUMP_PIN,      INPUT);

//L FALL/BUMPER SENSOR
  pinMode(L_FALL_PIN,      INPUT);
  pinMode(L_BUMP_PIN,      INPUT);

//R FALL/BUMPER SENSOR
  pinMode(R_FALL_PIN,      INPUT);
  pinMode(R_BUMP_PIN,      INPUT);

//B FALL/BUMPER SENSOR
  pinMode(B_FALL_PIN,      INPUT);
  pinMode(B_BUMP_PIN,      INPUT);

//F Ultrasound sensor
  pinMode(F_US_TRIG,      OUTPUT);
  pinMode(F_US_ECHO,      INPUT);
//L Ultrasound sensor
  pinMode(L_US_TRIG,      OUTPUT);
  pinMode(L_US_ECHO,      INPUT);
//R Ultrasound sensor
  pinMode(R_US_TRIG,      OUTPUT);
  pinMode(R_US_ECHO,      INPUT);
//B Ultrasound sensor
  pinMode(R_US_TRIG,      OUTPUT);
  pinMode(R_US_ECHO,      INPUT);
  
}

void enc_L_B (){
  l_pulses++;
}

void enc_R_B (){
  r_pulses++;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ANALIZAR CADENA~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void analizar_orden()
{  
  switch (orden[0]){
    
    case 49:                                                     //ASCII 49 '1' -> AVANZA A VELOCIDADES DISTINTAS.
      encoderDER = 0;
      encoderIZQ = 0;
      valor = leer_numero();
      velIZQ = valor;
      //Serial3.println("Velocidad de motor izquierdo");
      //Serial3.println(valor);
      valor = leer_numero();
      velDER = valor;
      //Serial3.println("Velocidad de motor derecho");
      //Serial3.println(valor);
      auxencoder = '0';
      auxbloqueo = '0';
      digitalWrite(MOT_R_B_PIN, HIGH);
      digitalWrite(MOT_L_B_PIN, HIGH);
      digitalWrite(MOT_R_A_PIN, LOW);
      digitalWrite(MOT_L_A_PIN, LOW);
      analogWrite(MOT_R_PWM_PIN, velDER);                                                                                        
      analogWrite(MOT_L_PWM_PIN, velIZQ);
      break;

     //ASCII 50 '2' -> Prueba calibracion avanzar.
      case 50:                                                         
      //Distancia D en mm
      D = 4000;
      D = D/K;
      
      //Serial3.println("Calibracion de avanzar");
      velDER = VEL_DER_REF_170;
      velIZQ = 170;
      encoderDER = 0;
      encoderIZQ = 0;
     
      //Arrancamos los motores a la vez
      //R-A -> PA4 ||| R-B -> PA0 ||| L-A -> PA6 ||| L-B -> PA2
      PORTA = PORTA & ~(B1010101); //Pone a LOW los pines A y B de los encoder L y R
      analogWrite(MOT_R_PWM_PIN, velDER);                                                                                        
      analogWrite(MOT_L_PWM_PIN, velIZQ);
      //HIGH R-B L-B
      PORTA = PORTA | B0000101;
      


  while(encoderIZQ<D)
  {
    error = encoderIZQ-encoderDER;  //calculate error values
    derror = (error - preverror);         // derivative error
    control = ((KP*error) + (KD*derror) + (KI*Ierror)); //PID equations
    if((velDER + control) > 255) //prevent OCR0 and OCR2 from overrunning 255
      velDER = 255;
    else if ((velDER+control)<0 )
        velDER=0;
    else
       {velDER = velDER + control; 
       //Serial3.println("Ajustamos velDER");
       }    //use output from PID equations to alter motor speeds
    
    preverror = error;          //set previous error to current error
    Ierror = Ierror + error;    //add current error to integral error
    
    analogWrite(MOT_R_PWM_PIN, velDER);
    delay(250);
    /*if (encoderIZQ>=D)
      {
      //Paramos los motores
      PORTA = PORTA & ~(B1010101);
      analogWrite(MOT_L_PWM_PIN,255);
      analogWrite(MOT_R_PWM_PIN, 255);
      }*/
  }
  
      //Paramos los motores
      analogWrite(MOT_R_PWM_PIN,255);
      analogWrite(MOT_L_PWM_PIN, 255);
      PORTA = PORTA | (B1010101);
      
      Serial3.print(")");
      Serial3.print(velIZQ);
      Serial3.print("*");
      Serial3.println(velDER);
      delay(500);
      Serial3.print("!");
      Serial3.print(encoderIZQ);
      Serial3.print("*");
      Serial3.println(encoderDER);
      encoderDER = 0;
      encoderIZQ = 0;
      
    break;

    case 51:                                                        //ASCII 51 '3' -> RETROCEDE A VELOCIDADADES DISTINTAS.
      encoderDER = 0;
      encoderIZQ = 0;
      valor = leer_numero();
      velIZQ = valor;
      auxencoder = '0';
      auxbloqueo = '1';
      valor = leer_numero();
      velDER = valor;
      digitalWrite(MOT_R_B_PIN, LOW);
      digitalWrite(MOT_L_B_PIN, LOW);
      digitalWrite(MOT_R_A_PIN, HIGH);
      digitalWrite(MOT_L_A_PIN, HIGH);
      analogWrite(MOT_R_PWM_PIN, velDER);                                                                                        
      analogWrite(MOT_L_PWM_PIN, velIZQ);
      break;

    case 52:                                                        // 52 -> '4' -> Nueva funcion avanzar recto. Se ejecuta en loop
      distancia = leer_numero();
      encoderDER = 0;
      encoderIZQ = 0;
      //Paso la distancia en cm, así que la convierto a mm
      D = distancia*10;
      limitencoder_avanzar = (D/K)-50; //Posible correcion en la frenada por el suelo resbaladizo (50 pulsos)
      auxvelocidad_avanzar = '1';
      
      //Serial3.println("Calibracion de avanzar");
      velDER = VEL_DER_REF_170;
      velIZQ = 170;
      encoderDER = 0;
      encoderIZQ = 0;

      auxencoder_avanzar = '1';
      //Arrancamos los motores a la vez
      //R-A -> PA4 ||| R-B -> PA0 ||| L-A -> PA6 ||| L-B -> PA2
      PORTA = PORTA & ~(B1010101); //Pone a LOW los pines A y B de los encoder L y R
      analogWrite(MOT_R_PWM_PIN, velDER);                                                                                        
      analogWrite(MOT_L_PWM_PIN, velIZQ);
      //HIGH R-B L-B
      PORTA = PORTA | B0000101;
      break;

            
            
    case 56:                                                        //ASCII 56 '8' -> GIRA RESPECTO RUEDA IZQUIERDA ADELANTE X GRADOS A VELOCIDAD DEFINIDA.
      valor = leer_numero();
      //Serial3.println("Grados a girar");
      //Serial3.println(valor);
      grados = valor;
      if(grados>360)
        grados = 360;
      if(grados<=0)
        grados = 0;
      else
      {      
        //Distancia entre ruedas
        L = DIST_RUEDAS;
        encoderDER = 0;
        encoderIZQ = 0;
        auxencoder = '1';
        auxbloqueo = '0';
        auxvelocidad ='1';
        //Longitud a recorrer (arco de la circunferencia = angulo en radianes x radio)
        s = (grados*(pi/180)) * L;
        limitencoder = (int)s/K;
        valor = leer_numero();
        //Serial3.println("Velocidad de giro");
        //Serial3.println(valor);
        velDER = valor;
        //limitencoder = (int)((grados * pulsosgradosgrande)+0.5f)-23;
        digitalWrite(MOT_R_B_PIN, HIGH);
        digitalWrite(MOT_L_B_PIN, HIGH);
        digitalWrite(MOT_R_A_PIN, LOW);
        digitalWrite(MOT_L_A_PIN, HIGH);
        analogWrite(MOT_R_PWM_PIN, velDER);
        analogWrite(MOT_L_PWM_PIN, 255);
      }
      break;

    case 57:                                               //ASCII 57 '9' -> GIRA RESPECTO RUEDA IZQUIERDA ATRAS X GRADOS A VELOCIDAD DEFINIDA.
      valor = leer_numero();
      grados = valor;
      if(grados>360)
        grados = 360;
      if(grados<0)
        grados = 0;
      else
      {
        //Distancia entre ruedas
        L = DIST_RUEDAS;
        encoderDER = 0;
        encoderIZQ = 0;
        auxencoder = '1';
        auxbloqueo = '0';
        auxvelocidad ='1';
        //Longitud a recorrer (arco de la circunferencia = angulo en radianes x radio)
        s = (grados*(pi/180)) * L;
        limitencoder = (int)s/K;
        valor = leer_numero();
        velDER = valor;
        //limitencoder = (int)((grados * pulsosgradosgrande)+0.5f)-23;
        digitalWrite(MOT_R_B_PIN, LOW);
        digitalWrite(MOT_L_B_PIN, HIGH);
        digitalWrite(MOT_R_A_PIN, HIGH);
        digitalWrite(MOT_L_A_PIN, HIGH);
        analogWrite(MOT_R_PWM_PIN, velDER);
        analogWrite(MOT_L_PWM_PIN, 255);
      }
      break;

    case 58:                                               //ASCII 58 ':' -> ESTADO DE LOS SENSORES.
      
      break;
      
    case 59:                                                    //ASCII 59 ';' -> ESTADO DE LOS ENCODERS.
      Serial3.print(")");
      Serial3.print(encoderIZQ);
      Serial3.print("*");
      Serial3.println(encoderDER);
      break;

    case 60:                                                   //ASCII 60 '<' -> RESET DE LA CUENTA DE AMBOS ENCODERS.
      encoderDER = 0;
      encoderIZQ = 0;
      //Serial3.println("'");
      break;

    case 63:                                                     //ASCII 63 '?' -> DETENER AMBOS MOTORES.
      auxencoder = 0;
      velDER = 0;
      velIZQ = 0;
      //analogWrite(pwmDER, 0);                                                                                            
      //analogWrite(pwmIZQ, 0);
      digitalWrite(MOT_R_B_PIN, LOW);
      digitalWrite(MOT_R_A_PIN, LOW);
      digitalWrite(MOT_L_B_PIN, LOW);
      digitalWrite(MOT_L_A_PIN, LOW);
      analogWrite(MOT_R_PWM_PIN, 255);                                                                                            
      analogWrite(MOT_L_PWM_PIN, 255);
      while(Serial3.available() > 0)
        Serial3.read();   
      break; 

    default:
      break;
  }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~LEER NUMERO~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
short int leer_numero()
{
  int val = 0;
  char velocidad[3];
  delay(5);
  Serial3.readBytes(velocidad, 3);
  
  val = atoi(velocidad);
  return int(val);                                                                                                             //Devuelve la variable VAL
}


