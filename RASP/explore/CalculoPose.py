'''
Created on Jan 24, 2018

@author: pedro
'''

import zbar
import cv2
import numpy

def PintarEsquinas(img, pos):
    cv2.circle(img, (pos[0][0], pos[0][1]), 3, (0xFF, 0x00, 0x00), -1)
    cv2.circle(img, (pos[1][0], pos[1][1]), 3, (0x00, 0xFF, 0x00), -1)
    cv2.circle(img, (pos[2][0], pos[2][1]), 3, (0x00, 0x00, 0xFF), -1)
    cv2.circle(img, (pos[3][0], pos[3][1]), 3, (0xFF, 0xFF, 0x00), -1)
    
    
def CalculoPoseQR(img, puntosQR, K):
    '-img: Imagen visualizando el código QR'
    '-tam: Tamaño en el papel del còdigo QR'
    scanner=zbar.Scanner()
    try:
        if img.shape[2]==3:
            gray=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        else:
            gray=img
    except IndexError:
        gray=img
         
    res=scanner.scan(gray)
    if len(res)<1:
        raise ValueError
    pos=res[0][3]
    PintarEsquinas(img, pos)
    puntosImagen=numpy.matrix([
        [pos[0][0], pos[0][1]],
        [pos[1][0], pos[1][1]],
        [pos[2][0], pos[2][1]],
        [pos[3][0], pos[3][1]]],
        numpy.float32)
    res, r1, t1=cv2.solvePnP(puntosQR, puntosImagen, K, None)
    return r1, t1

def PlanoResultados(size, dist):
    img=numpy.zeros( (size[1], size[0], 3), numpy.uint8)
    cv2.circle(img, (0, size[1]//2), 4, (0xFF, 0x00, 0x00, -1))
    x=0
    while True:
        x+=dist
        if x>size[0]: break
        cv2.line(img, (x, 0), (x, size[1]), (0x00, 0xFF, 0x00), 1)
    c=size[1]//2
    y=0
    while True:
        if y>c: break
        cv2.line(img, (0, c+y), (size[0], c+y), (0x00, 0xFF, 0x00), 1)
        cv2.line(img, (0, c-y), (size[0], c-y), (0x00, 0xFF, 0x00), 1)
        y+=dist
    return img
    
def PintarPose(plano, R, C, escala, color, ancho):
    p=( int(escala*C[2, 0]), int(escala*C[0, 0]+plano.shape[0]/2) )
    cv2.circle(plano, p, 6, color, -1)
    v=-R[:, 2]
    e=( int(100*v[2]+p[0]), int(100*v[0]+p[1]) )
    cv2.line(plano, p, e, color, ancho)
