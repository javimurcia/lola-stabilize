#!/usr/bin/env python3

import pygame
from io import BytesIO
from boto3 import Session
from botocore.exceptions import BotoCoreError, ClientError
from contextlib import closing
import hashlib
from os import path
import sys
import codecs


class VoiceSynthesizer(object):
    def __init__(self, volume=0.1, TextType='text', tts_online=False):
        # Configure the audio system
        pygame.mixer.init()
        self._volume = volume

        # where are the audio responses stored
        cache_dir = "/polly_tts/speech_cache/"
        self.cache_dir = path.dirname(path.realpath(sys.argv[0])) + cache_dir

        # Connect to amazon
        if tts_online:
            try:
                session = Session(profile_name="default")
                self.__polly = session.client("polly")
                self._TextType = TextType
                self.tts_online = True
            except:
                print("ERROR: No se ha podido conectar con AWS Polly")
                print("TTS: Funcionando en modo OFFLINE")
                self.tts_online = False
        else:
            print("TTS: Funcionando en modo OFFLINE")
            self.tts_online = False

    def _getVolume(self):
        return self._volume

    def say(self, text):
        # Calculate speech hash
        hash = hashlib.md5(text.encode('utf-8')).hexdigest()
        fname = self.cache_dir + hash + '.ogg'
        if path.isfile(fname):
            # file exists, play it
            with open(fname, 'rb') as file:
                sound = pygame.mixer.Sound(file=file)
                sound.set_volume(self._getVolume())
                sound.play()
        else:
            # File doesn't exists, request tts to amazon
            if self.tts_online:
                self._synthesize(text)
                if path.isfile(fname):
                    with open(fname, 'rb') as file:
                        sound = pygame.mixer.Sound(file=file)
                        sound.set_volume(self._getVolume())
                        sound.play()
            else:
                self.say(
                    '<speak><prosody volume="x-loud">Error.</prosody> Ha habido un problema al conectarse al servicio <lang xml:lang="en-UK">online</lang> de <lang xml:lang="en-UK">text to speech</lang></speak>')

    def say_online (self, text):
        try:
            # Request speech synthesis
            response = self.__polly.synthesize_speech(Text=text, TextType=self._TextType,
                                                      OutputFormat="ogg_vorbis", VoiceId="Conchita")
        except (BotoCoreError, ClientError) as error:
            # The service returned an error
            print(error)
            '''exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=5, file=sys.stdout)'''
            self.say(
                '<speak><prosody volume="x-loud">Error.</prosody> Ha habido un problema al conectarse al servicio <lang xml:lang="en-UK">online</lang> de <lang xml:lang="en-UK">text to speech</lang></speak>')
            return

            # Note: Closing the stream is important as the service throttles on the
            # number of parallel connections. Here we are using contextlib.closing to
            # ensure the close method of the stream object will be called automatically
            # at the end of the with statement's scope.
            with closing(response["AudioStream"]) as stream:
                data = stream.read()
                filelike = BytesIO(data)  # Gives you a file-like object
                sound = pygame.mixer.Sound(file=filelike)
                sound.set_volume(self._getVolume())
                sound.play()

    def _synthesize(self, text):
        # Implementation specific synthesis
        try:
            # Request speech synthesis
            response = self.__polly.synthesize_speech(Text=text, TextType=self._TextType,
                                                      OutputFormat="ogg_vorbis", VoiceId="Conchita")
        except (BotoCoreError, ClientError) as error:
            # The service returned an error
            print(error)
            '''exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=5, file=sys.stdout)'''
            self.say(
                '<speak><prosody volume="x-loud">Error.</prosody> Ha habido un problema al conectarse al servicio <lang xml:lang="en-UK">online</lang> de <lang xml:lang="en-UK">text to speech</lang></speak>')
            return

        # Compute the speech hash, create a file to store it
        hash = hashlib.md5(text.encode('utf-8')).hexdigest()
        fname = self.cache_dir + hash + '.ogg'

        # Access the audio stream from the response
        if "AudioStream" in response:
            # Note: Closing the stream is important as the service throttles on the
            # number of parallel connections. Here we are using contextlib.closing to
            # ensure the close method of the stream object will be called automatically
            # at the end of the with statement's scope.
            with closing(response["AudioStream"]) as stream:
                # Get the adiostream from amazon, and store in a file
                with open(fname, 'wb') as file:
                    data = stream.read()
                    file.write(data)
                    print("TTS: saved as " + fname)
                # Also, write to a list the hash and the corresponding speech
                with codecs.open(self.cache_dir + "speech_list.txt", "ab", "utf-8") as myfile:
                    myfile.write(hash + " \t " + text + '\n')
        else:
            # The response didn't contain audio data, exit gracefully
            print("Could not stream audio - no audio data in response")

    def IsSpeaking(self):
        return pygame.mixer.get_busy()


if __name__ == "__main__":
    import sys, traceback

    # Test code
    debugging = False
    try:
        synthesizer = VoiceSynthesizer(1.0, TextType='ssml')
        if len(sys.argv) > 1:
            if sys.argv[1] == '-s':
                synthesizer = VoiceSynthesizer(1.0, TextType='ssml')
                synthesizer.say(str(sys.argv[2]))
            else:
                synthesizer = VoiceSynthesizer(1.0, TextType='text')
                synthesizer.say(str(sys.argv[1]))
    except:
        print ("exception occurred!")
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=5, file=sys.stdout)
