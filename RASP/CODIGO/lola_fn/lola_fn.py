#!/usr/bin/python2
# -*- coding: utf-8 -*-

'''
 Autor = Jesús Gómez
 Fecha = 06/05/2018

 Novedades y ajustes: 
    - Incorporado para cerrar ventana OpenCV con la cruz (Hay que probar en RASP)
	- Arreglado el envio de email en caso de no estar conectado al Wifi
	- 
	- Juego Casi arreglado Ver problemas con Satur
'''

# Ui framework
import Tkinter as tk
import sys, os, time, pygame, re, math, subprocess, datetime, calendar, commands
from pygame.locals import *

from Tkinter import Frame, Button, Label, PhotoImage, Toplevel, Entry, StringVar, Tk, Text, END, Scrollbar
import ttk
import random, imghdr
import serial

from PIL import Image, ImageTk

import smtplib
import Tkinter, tkFileDialog, tkFont, tkMessageBox
import email.mime.application
from tkMessageBox import *

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage

import cv2
import zbar
import CalculoPose
import matplotlib.pyplot as plt
import numpy as np

import threading
import ShockCalendar
import Queue #Para alarma

import SocketServer
#import picamera
#import picamera.array

reload(sys)  
sys.setdefaultencoding('utf8')
files = list()

cola = Queue.Queue()

#Botones para el teclado
tecla = [ 

'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
'z', 'x', 'c', 'v', 'b', 'n', 'm','BACK','SPACE'
]



ShockCalendar.colors = {
              "background": "#DF013A",
              "foreground": '#F4FA58',
              "daybg": '#F4FA58',
              "dayfg": "red",
              "linescolor": "#AAAAAA",
              "outrangedaysfg": "gray",
              "currentdayfg": "blue",
              "currentdayoutline": "blue",
              "selectdatebg": "#DDDDDD",
              "selectdatelinecolor": "red",
              "eventday": "violet"
        }


#Constantes para el juego
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 480

#Variables para el calendario
year = datetime.date.today().year
mes = datetime.date.today().month

# Settings for the web-page
webPort = 80                            # Port number for the web-page, 80 is what web-pages normally use
imageWidth = 240                        # Width of the captured image in pixels
imageHeight = 180                       # Height of the captured image in pixels
frameRate = 10                          # Number of images to capture per second
displayRate = 2                         # Number of images to request per second
photoDirectory = '/home/desarrollo/Images'             # Directory to save photos to

# Global values
global lastFrame
global lockFrame
global camera
global processor
global running
global watchdog
running = True

#Avance lineal por pulso del encoder en mm
cm = 0.2110

#Q=funcionQR.QR()
#Inicializaciones
escala=3 #pixeles/cm
marca=40 #cm/marca
plano=CalculoPose.PlanoResultados((600, 400), escala*marca)
d=2.5
puntos=np.matrix([
    [-d, -d, 0],
    [-d, +d, 0],
    [+d, +d, 0],
    [+d, -d, 0]], np.float32)

#Start posicion robot 
ANG=0
X=0
Y=0
enqIzq=0
enqDer=0

ser = 0
MAPSCALE = 200
anguloCamino = 0
i = 0
Optimal_path = 0

QRNames = ['S34','S345','S344','S346','S343','S347','S342','S348','S341','SH','SM','PFP','VB1','VB2','ALM1','SE','AUX1','FCP','CEL1','CEL2']

QR_X = [114,108,103,90,86,73,68,60,55,44,44,26,7,7,22,17,23,14,39,42]
QR_Y = [68,72,66,72,64,71,64,72,65,72,65,64,67,71,91,92,42,36,41,22]


class cuento:
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        image_file = self.path + "lola_fn/imagenes/cuento/cuento.gif"

        # paint the screen
        self.image = tk.PhotoImage(file=image_file)
        self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="blue")
        self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

        # Bind the screen clic to the "handler" method
        self.canvas.bind("<Button-1>",  self.handler)

        # prepare the screen
        self.canvas.pack()

        # update the screen (what the uses sees)
        self.lola.aux_windows[0].update()

    def handler(self, event):
        pygame.mixer.music.stop()
        self.lola.aux_windows[0].destroy()

    def run(self, actions=None):
        #make sure the TTS is quiet
        while self.lola.synthesizer.IsSpeaking():
            pass
        # prepare the audio subsystem
        pygame.mixer.init()
        pygame.mixer.music.load(self.path + 'lola_fn/sonidos/cuento/cuento.ogg')

        # start playing the audio
        pygame.mixer.music.play()

        # Wait until there is no audio playing
        while pygame.mixer.music.get_busy():
            self.lola.aux_windows[0].update()
            pygame.time.Clock().tick(10)

class explore:
    def __init__(self,lola):
		# copy the lola object to self.lola, so it can be used later
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        image_file = self.path + "lola_fn/padrino_v.png"

        # paint the screen
        self.image = tk.PhotoImage(file=image_file)
        self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800)
        self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

        # Bind the screen clic to the "handler" method
        #self.canvas.bind("<Button-1>",  self.handler)

        # prepare the screen
        self.canvas.pack()

	# update the screen (what the uses sees)
        self.lola.aux_windows[0].update()
    def run(self, actions=None):
        os.system('python2 ~/a/explore/exploracion.py')
        #Cambiar ruta



class Musica:
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        image_file = self.path + "lola_fn/torre-eiffel.gif"

        # paint the screen
        self.image = tk.PhotoImage(file=image_file)
        self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="blue")
        self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

        # Bind the screen clic to the "handler" method
        self.canvas.bind("<Button-1>",  self.handler)

        # prepare the screen
        self.canvas.pack()

        # update the screen (what the uses sees)
        self.lola.aux_windows[0].update()

    def handler(self, event):
        pygame.mixer.music.stop()
        self.lola.aux_windows[0].destroy()

    def run(self, actions=None):
        #make sure the TTS is quiet
        while self.lola.synthesizer.IsSpeaking():
            pass
        # prepare the audio subsystem
        pygame.mixer.init()
        pygame.mixer.music.load(self.path + 'lola_fn/a.ogg')

        # start playing the audio
        pygame.mixer.music.play()

        # Wait until there is no audio playing
        while pygame.mixer.music.get_busy():
            self.lola.aux_windows[0].update()
            pygame.time.Clock().tick(10)

class Marco:
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        self.image_file1 = self.path + "lola_fn/torre-eiffel.gif"
        self.image_file2 = self.path + "lola_fn/torre-eiffel2.gif"

        # paint the screen
        self.image1 = tk.PhotoImage(file=self.image_file1)
        self.image2 = tk.PhotoImage(file=self.image_file2)
        self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="blue")
        self.image_on_canvas =self.canvas.create_image(800 / 2, 480 / 2, image=self.image1)

        # Bind the screen clic to the "handler" method
        self.canvas.bind("<Button-1>",  self.handler)

        # prepare the screen
        self.canvas.pack()

        # update the screen (what the uses sees)
        self.lola.aux_windows[0].update()

        self.flag=False

    def handler(self, event):

        # Create a TTS object for speech synthesis
        self.lola.synthesizer.say("<speak>Cerramos Marco dos.</speak>")
        print(event.x,event.y)

        self.flag=True
        print(self.flag)
        self.lola.aux_windows[0].destroy()


    def run(self, actions=None):        
        start = time.time()            
        self.canvas.itemconfig(self.image_on_canvas, image=self.image2)
        self.lola.aux_windows[0].update()
        contador=1;
        while not self.flag:                
            end = time.time()
            self.lola.aux_windows[0].update()
            if end-start>10:
                print(end - start)
                if (contador==1):
                    self.canvas.itemconfig(self.image_on_canvas, image=self.image1)
                    self.lola.aux_windows[0].update()
                    contador=-1
                else:
                    self.canvas.itemconfig(self.image_on_canvas, image=self.image2)
                    self.lola.aux_windows[0].update()
                    contador=1
                start = time.time()


class Juego:
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola
        
        # Set config vars
        self.IMG_NUM = 4
        self.WAIT_TIME = 10  # Tiempo entre dos imágenes
        # Set start iteration
        self.iteration = 0

        # Set variable to calculate the time
        self.start_time = 0

        # Set bool to let the user click only once
        self.answered = False

        self.use = []
        # Create a list for the image paths, canvas images and response times
        self.imgs = []
        self.images = []
        self.img_corr_inc = []
        self.times  = []
        
        # Setup image paths (Checks that there are no repeated imgs)
        img_path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/lola_fn/images/'
        for i in range(self.IMG_NUM):
            added_flag = True
            # Search valid image
            while(added_flag):
                img_to_add = random.choice(os.listdir(img_path))
                print(img_to_add)

                # Check that the new image:
                #  its not in the list, its a gif, has either 'b' or 'd'
                if(     imghdr.what(img_path+img_to_add) == 'gif'
                        and img_to_add not in self.imgs
                        and ((img_to_add.find('b') != -1) is not (img_to_add.find('d') != -1))
                   ):
                    print(img_to_add)
                    self.imgs.append(img_to_add)
                    added_flag = False
            
        '''
        #test
        print('Choosen IMGS: ' + self.imgs[0] + ', ' + self.imgs[1] + ', ' + self.imgs[2] + ', ' + self.imgs[3] + ', ' + self.imgs[4])
        #'''  

        # get the program's PATH so we can find the sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'

        # Prepares the images and then paints the screen
        #  this loop does the same as the code below but its more modular
        for i in range(self.IMG_NUM):
            self.images.append(tk.PhotoImage(file= img_path + self.imgs[i]))
        self.img_corr_inc.append(tk.PhotoImage(file= img_path + "Correcto.gif"))
        self.img_corr_inc.append(tk.PhotoImage(file= img_path + "Incorrecto.gif"))
        
        '''self.image_file1 = img_path + self.imgs[0]
        self.image_file2 = img_path + self.imgs[1]
        self.image_file3 = img_path + self.imgs[2]
        self.image_file4 = img_path + self.imgs[3]
        self.image_file5 = img_path + self.imgs[4]
        
        # paint the screen
        self.images[0] = tk.PhotoImage(file=self.image_file1)
        self.images[1] = tk.PhotoImage(file=self.image_file2)
        self.images[2] = tk.PhotoImage(file=self.image_file3)
        self.images[3] = tk.PhotoImage(file=self.image_file4)
        self.images[4] = tk.PhotoImage(file=self.image_file5)
        '''
        
        self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="blue")
        self.image_on_canvas = self.canvas.create_image(800 / 2, 480 / 2, image=self.images[0])
        
        # Bind the screen clic to the "handler" method
        self.canvas.bind("<Button-1>",  self.handler)

        # prepare the screen
        self.canvas.pack()

        # update the screen (what the uses sees)
        self.lola.aux_windows[0].update()

        self.flag=False
   
       

    def handler(self, event):

        # Create a TTS object for speech synthesis
        # self.lola.synthesizer.say("<speak>Cerramos Juego 1.</speak>")
        #print(event.x,event.y)
        
        tiempo_pasado=float(time.time()) - self.start_time

        print(tiempo_pasado)
        if (tiempo_pasado<0.1):
            return

        #''' if user clicked B
        if(event.x < 230 and not self.answered):
            if(self.imgs[self.iteration].find('b') != -1):
                #print("correct")
                self.lola.synthesizer.say("<speak>CORRECTO</speak>")
                # Save response time
                print("time: " + str(float(time.time()) - self.start_time))
                self.times.append(float(time.time()) - self.start_time)
                self.flag=True
            else:
                #print("incorrect")
                self.lola.synthesizer.say("<speak>NO ES CORRECTO</speak>")
                self.times.append(self.WAIT_TIME)
                self.flag=False

                
        # if user clicked D    
        elif(event.x > 570 and not self.answered):
            if(self.imgs[self.iteration].find('d') != -1):
                #print("correct")
                self.lola.synthesizer.say("<speak>CORRECTO</speak>")
                # Save response time
                print("time: " + str(float(time.time()) - self.start_time))
                self.times.append(float(time.time()) - self.start_time)
                self.flag=True
            else:
                #print("incorrect")
                self.lola.synthesizer.say("<speak>NO ES CORRECTO</speak>")
                self.times.append(self.WAIT_TIME)
                self.flag=False
                
        # if user didnt click any:
        #elif(not self.answered):
        #     self.flag=True

        self.answered = True
        #'''
        #print(self.flag)
        #self.lola.aux_windows[0].destroy()

    
    def run(self, actions=None):        

        '''start = time.time()'''           
        self.canvas.itemconfig(self.image_on_canvas, image=self.images[0])
        self.lola.aux_windows[0].update()
        time.sleep(2)
        self.lola.synthesizer.say("<speak>Tienes que tocar la letra con la que se escribe cada una de las palabras que aparecen en las imágenes</speak>")
        time.sleep(7)
        contador=1

        self.start_time = time.time()
        #while(self.iteration < self.IMG_NUM and not self.flag):
        while(self.iteration < self.IMG_NUM):
            # Set flag down
            self.answered = False
            # Save start time
            # Change image
            #print(self.imgs[self.iteration])
            self.canvas.itemconfig(self.image_on_canvas, image=self.images[self.iteration])
            self.lola.aux_windows[0].update()

            self.start_time = time.time()
            self.lola.synthesizer.say("<speak>Con que se escribe " + self.imgs[self.iteration]+"</speak>")

            self.start_time = time.time()
            #print("startime: " + str(self.start_time))
            
            for i in range(400):
                #time.sleep(self.WAIT_TIME/10)
                time.sleep(0.01)
                self.lola.aux_windows[0].update()
                if (self.answered):
                    if (self.flag==True):
                      self.canvas.itemconfig(self.image_on_canvas, image=self.img_corr_inc[0])
                    else:
                      self.canvas.itemconfig(self.image_on_canvas, image=self.img_corr_inc[1])
                    self.lola.aux_windows[0].update()
                    time.sleep(1)
                    break
            # If not answered save the max time
            if(not self.answered):
                self.times.append(self.WAIT_TIME)
                self.canvas.itemconfig(self.image_on_canvas, image=self.img_corr_inc[1])
                self.lola.aux_windows[0].update()
                self.lola.synthesizer.say("<speak>Tiempo superado</speak>")
                time.sleep(1)

            # Set flag down
            #self.flag = False
            
            # Update iteration
            self.iteration += 1

        #for i in self.times:
        print(self.times)
        self.times=sorted(self.times,reverse=True)
        plt.subplot(312)
        plt.plot(self.times)
        plt.ylabel('Tiempo de respuesta (seg.)')
        plt.xlabel('Indice de respuestas ordenadas por tiempo consumido');
        plt.savefig('./lola_fn/images/use.png')

        img_path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/lola_fn/images/'



        self.use.append(tk.PhotoImage(file= img_path + "use.png"))
        self.canvas.itemconfig(self.image_on_canvas, image=self.use[-1])
        self.lola.aux_windows[0].update()
        time.sleep(4)

        self.lola.aux_windows[0].destroy()



################################################################################################
################################    CLASE PARA EL JUEGO      ###################################
################################################################################################

class Pelota(pygame.sprite.Sprite):
    def __init__(self, sonido_golpe, sonido_punto):
        pygame.sprite.Sprite.__init__(self)
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        filename = self.path + "lola_fn/imagenes/juego/bola.png"
        self.image = pygame.image.load(filename)
        self.rect = self.image.get_rect()
        self.rect.centerx = SCREEN_WIDTH / 2
        self.rect.centery = SCREEN_HEIGHT / 2
        self.speed = [3, 3]
        self.sonido_golpe = sonido_golpe
        self.sonido_punto = sonido_punto

    def update(self):
        if self.rect.left < 0 or self.rect.right > SCREEN_WIDTH:
            self.speed[0] = -self.speed[0]
            #self.sonido_punto.play()  # Reproducir sonido de punto
            self.rect.centerx = SCREEN_WIDTH / 2
            self.rect.centery = SCREEN_HEIGHT / 2
            pygame.mixer.music.load(self.path + 'lola_fn/sonidos/juego/aplausos.ogg')
            pygame.mixer.music.play()
        if self.rect.top < 0 or self.rect.bottom > SCREEN_HEIGHT:
            self.speed[1] = -self.speed[1]
        self.rect.move_ip((self.speed[0], self.speed[1]))

    def colision(self, objetivo):
        if self.rect.colliderect(objetivo.rect):
            self.speed[0] = -self.speed[0]
            pygame.mixer.music.load(self.path + 'lola_fn/sonidos/juego/tennis.ogg')
            pygame.mixer.music.play()



class Paleta(pygame.sprite.Sprite):
    def __init__(self, x):
        pygame.sprite.Sprite.__init__(self)
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        filename = self.path + "lola_fn/imagenes/juego/paleta.png"
        self.image = pygame.image.load(filename)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.centery = SCREEN_HEIGHT / 2

    def humano(self):
        # Controlar que la paleta no salga de la pantalla
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT
        elif self.rect.top <= 0:
            self.rect.top = 0

    def cpu(self, pelota):
        self.speed = [0, 2.5]
        if pelota.speed[0] >= 0 and pelota.rect.centerx >= SCREEN_WIDTH / 2:
            if self.rect.centery > pelota.rect.centery:
                self.rect.centery -= self.speed[1]
            if self.rect.centery < pelota.rect.centery:
                self.rect.centery += self.speed[1]


class juego:

    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        #self.lola.aux_windows[0].destroy()


        self.juego()

    def juego(self):
        self.done = False
        pygame.init()
        pygame.mixer.init()

        # creamos la ventana y le indicamos un titulo:
        screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
        pygame.display.set_caption("Ping Pong")
        # cargamos el fondo y la musica

        try:
            filename = self.path + "lola_fn/imagenes/juego/fondo.jpg"
            fondo = pygame.image.load(filename)
            fondo = fondo.convert() 
        except pygame.error as e:
            print('No se puede abrir la imagen: ', filename)
            raise SystemExit(str(e))

        sonido_golpe = pygame.mixer.music.load(self.path + 'lola_fn/sonidos/juego/tennis.ogg')
        sonido_punto = pygame.mixer.music.load(self.path + 'lola_fn/sonidos/juego/aplausos.ogg')

        bola = Pelota(sonido_golpe, sonido_punto)
        jugador1 = Paleta(40)
        jugador2 = Paleta(SCREEN_WIDTH - 40)

        clock = pygame.time.Clock()
        pygame.key.set_repeat(1, 25)  # Activa repeticion de teclas
        pygame.mouse.set_visible(False)

        # el bucle principal del juego
        while True:
            clock.tick(60)
            # Obtenemos la posicon del mouse
            pos_mouse = pygame.mouse.get_pos()
            mov_mouse = pygame.mouse.get_rel()

            # Actualizamos los obejos en pantalla
            jugador1.humano()
            jugador2.cpu(bola)
            bola.update()

            # Comprobamos si colisionan los objetos
            bola.colision(jugador1)
            bola.colision(jugador2)

            # actualizamos la pantalla
            screen.blit(fondo, (0, 0))
            todos = pygame.sprite.RenderPlain(bola, jugador1, jugador2)
            todos.draw(screen)
            pygame.display.flip()

        # Posibles entradas del teclado y mouse
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    print "Hola"
                    self.done = True
                    break
                elif event.type == pygame.KEYDOWN:
                    if event.key == K_UP:
                        jugador1.rect.centery -= 5
                    elif event.key == K_DOWN:
                        jugador1.rect.centery += 5
                    elif event.key == K_ESCAPE:
                        self.done = True
                        pygame.quit() #Preguntar a Satur si sabe la diferencia entre cerrar ventana con cruz o con ESC ?¿
                elif event.type == pygame.KEYUP:
                    if event.key == K_UP:
                        jugador1.rect.centery += 0
                    elif event.key == K_DOWN:
                        jugador1.rect.centery += 0
                # Si el mouse no esta quieto, mover la paleta a su posicion
                elif mov_mouse[1] != 0:
                    jugador1.rect.centery = pos_mouse[1]


            if self.done == True:
                break
        pygame.quit()


    def run(self, actions=None):
        self.lola.aux_windows[0].destroy()
   
    def cerrar(self):
        self.lola.aux_windows[0].quit()
        self.lola.aux_windows[0].withdraw()
        


################################################################################################
################################    CLASE PARA EL CORREO       #################################
################################################################################################
            
        
class Email():
    def __init__(self, lola):
        self.lola = lola
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        ########### VENTANA PPAL  #################
        self.lola.aux_windows[0].title('Correo') 
        self.lola.aux_windows[0].geometry("800x480")
        self.lola.aux_windows[0].configure(background='#A9F5E1')
        #################################################

        ###############VENTANA TECLADO  #################
        self.win_teclado = Toplevel(self.lola.aux_windows[0])
        self.win_teclado.title("Teclado")
        self.win_teclado.resizable(0,0)#Para que la ventana tenga tamaño fijo
        ###############################################

        self.cuenta = 'robot@tecnoidea.es'
        self.password = 'Mail456rob$'
 
        self.server = smtplib.SMTP('mail.tecnoidea.es:26')
        self.server.starttls()
        self.server.login(self.cuenta,self.password)

        correo_dest= StringVar()
        correo_asunto = StringVar()

        Label(self.lola.aux_windows[0], text="De: %s" %self.cuenta, font=('Times', '-20','bold'), bg='#A9F5E1',fg='#FF0000').place(x=5, y=15)

        Label(self.lola.aux_windows[0], text="A:" , font=('Times', '-20','bold'), bg='#A9F5E1',fg='#FF0000').place(x=5, y=50)
        self.email_to = Entry(self.lola.aux_windows[0], textvariable=correo_dest, width=25)
        self.email_to.bind("<Button-1>", lambda e: self.teclado(1))
        self.email_to.place(x=5, y=75)

        Label(self.lola.aux_windows[0], text="Asunto:", font=('Times', '-20','bold'), bg='#A9F5E1',fg='#FF0000').place(x=5, y=100)
        self.email_asunto = Entry(self.lola.aux_windows[0], textvariable=correo_asunto, width=25)
        self.email_asunto.bind("<Button-1>", lambda e: self.teclado(2)) 
        self.email_asunto.place(x=5, y=125)

        Label(self.lola.aux_windows[0], text="Mensaje:", font=('Times', '-20','bold'), bg='#A9F5E1',fg='#FF0000').place(x=5, y=150)
        self.email_msg = Text(self.lola.aux_windows[0], width = 30, height = 15)
        self.email_msg.bind("<Button-1>", lambda e: self.teclado(3)) 
        self.email_msg.place(x=5, y=175)

        self.image = PhotoImage(file=self.path + "lola_fn/imagenes/correo/adjuntar.png")
        self.boton_archivo = Button(self.lola.aux_windows[0], image = self.image , command=self.add_file, bg='#A9F5E1').place(x=300, y=100)

        self.image1 = PhotoImage(file=self.path + "lola_fn/imagenes/correo/borrar.png")
        self.boton_borrar = Button(self.lola.aux_windows[0], image = self.image1, command=self.borrar, bg='#A9F5E1').place(x=450, y=100)

        self.image2 = PhotoImage(file=self.path + "lola_fn/imagenes/correo/enviar.png")
        self.boton_envio = Button(self.lola.aux_windows[0], image = self.image2, command=self.enviar, bg='#A9F5E1').place(x=300, y=240)

        self.image3 = PhotoImage(file=self.path + "lola_fn/imagenes/correo/salir.png")
        self.boton_salir = Button (self.lola.aux_windows[0], image = self.image3, command=self.salir, bg='#A9F5E1').place(x=450, y=240)


        #####################################
        ### Funcion borrar cuadros entry  ###
        #####################################
    def salir(self):
        self.win_teclado.withdraw()
        self.lola.aux_windows[0].withdraw() #Así al pulsar el boton sale al menú principal. Cierro la ventana del menú qr
        self.lola.aux_windows[0].quit()

    def borrar(self):
        self.email_to.delete(0, END)
        self.email_asunto.delete(0, END)
        self.email_msg.delete("1.0", END)

        #####################################
       ### Funcion boton adjuntar archivo  ###
        #####################################

    def add_file(self):
        file = tkFileDialog.askopenfile(parent=self.lola.aux_windows[0], mode="rb", title="Adjuntar archivo")
        if file != None:
            files.append(file.name)
            file.close()
        else:
            pass
        

    def enviar(self):
        self.to = self.email_to.get() #Destinatario
        
        msg = MIMEMultipart()
        msg['To'] = self.email_to.get()
        msg['From'] = 'robot@tecnoidea.es'
        msg['Subject'] = self.email_asunto.get()
        msg.attach(MIMEText(self.email_msg.get("1.0", END)))

        for file in files: #Archivo
            fp = open(file, "rb")
            if fp != None:
                slash = fp.name.split("/")
                name = slash[-1].lower()
                dot = name.split(".")
                ext = dot[-1].lower()
                self.archivo = email.mime.application.MIMEApplication(fp.read(),_subtype=ext)
                self.archivo.add_header("Content-Disposition","attachment",filename=name)
                fp.close()
            else:
                pass

        msg.attach(self.archivo)

        try:
        	self.server.sendmail(self.cuenta, self.to, msg.as_string())
        	self.server.quit()
        except Exception as e:
            tkMessageBox.showerror("No se ha podido conectar a la cuenta", str(e))
            self.salir()

    def run(self, actions=None):
        self.lola.aux_windows[0].mainloop()

    #####################################
    ##### Funciones para el teclado #####
    #####################################

    '''
       Aqui le paso el valor que corresponda a lambda e: self.teclado(n). 
       n=1 campo entry de escribir el email del destinatario
       n=2 campo entry de escribir el asunto 
       n=3 campo entry de escribir el mensaje
    '''

    def select_tecla(self, value, n):

        if n == 1: 
            if value == "BACK":
                self.email_to.delete(len(self.email_to.get())-1,Tkinter.END)
            elif value == "SPACE":
                self.email_to.insert(Tkinter.END, ' ')
            elif value == "->":
                self.email_to.insert(Tkinter.END, '    ')
            else :
                self.email_to.insert(Tkinter.END,value)
        if n == 2:
            if value == "BACK":
                self.email_asunto.delete(len(self.email_asunto.get())-1,Tkinter.END)
            elif value == "SPACE":
                self.email_asunto.insert(Tkinter.END, ' ')
            elif value == "->":
                self.email_asunto.insert(Tkinter.END, '    ')
            else :
                self.email_asunto.insert(Tkinter.END,value)
        if n == 3:
            if value == "BACK":
                self.email_msg.delete("1.0", END)
            elif value == "SPACE":
                self.email_msg.insert(Tkinter.END, ' ')
            elif value == "->":
                self.email_msg.insert(Tkinter.END, '    ')
            else :
                self.email_msg.insert(Tkinter.END,value)
    '''
       Funcion para pintar el teclado. Aqui le doy forma 
       n=1 campo entry de escribir el email
       n=2 campo entry de escribir la contraseña
       En la funcion self.select_tecla(x, n)
		- x hace referencia a value
		- n hace referencia al campo entry descrito anteriormente
    '''

    def teclado(self, n):

        varRow = 2
        varColumn = 0
        for button in tecla: 
            self.commando = lambda x=button: self.select_tecla(x, n) 
            if button == "SPACE" or button == "BACK"or button == "->" :
                Button(self.win_teclado,text= button,width=4, padx=1, pady=1, bd=3, bg="#9EB9D4", font=('Arial', '10','bold'), activebackground = "#ffffff", activeforeground="#000990", relief='raised',command=self.commando).grid(row=varRow,column=varColumn)
            else:
                Button(self.win_teclado,text= button,width=4, padx=1, pady=1, bd=6, bg="#9EB9D4", font=('Arial', '12','bold'), activebackground = "#ffffff", activeforeground="#000990", relief='raised',command=self.commando).grid(row=varRow,column=varColumn)

            varColumn +=1 

            if varColumn > 13 and varRow == 2:
                varColumn = 0
                varRow+=1
            if varColumn > 13 and varRow == 3:
                varColumn = 0
                varRow+=1
            if varColumn > 13 and varRow == 4:
                varColumn = 0
                varRow+=1
            if varColumn > 13 and varRow == 5:
                varColumn = 0
                varRow+=1
            if varColumn > 13 and varRow == 6:
                varColumn = 0
                varRow+=1
            if varColumn > 13 and varRow == 7:
                varColumn = 0
                varRow+=1

################################################################################################
################################    CLASE PARA LA ALARMA       #################################
################################################################################################

class Alarma: 
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola
 

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'

        #Todo esto es para la alarma

        self.lola.aux_windows[0].title('Alarma') 
        self.lola.aux_windows[0].geometry("800x480")
        self.lola.aux_windows[0].configure(background='#A9F5E1')

        Label(self.lola.aux_windows[0], text="Alarma de LOLA:", font=('Arial', '40'), bg='#A9F5E1',fg='#FF0000').place(x=150, y=0)
        self.etiqueta1=Label(self.lola.aux_windows[0], text='Hora de alarma:', font=('Arial', '22'), bg='#A9F5E1',fg='#FF0000')
        self.etiqueta1.place(x=5, y=100)
        self.hora = Entry(self.lola.aux_windows[0])
        self.hora.place(x=225, y=110)

        self.Mensaje_alarma= Label(self.lola.aux_windows[0], text='Mensaje de alarma:', font=('Arial', '22'), bg='#A9F5E1',fg='#FF0000')
        self.Mensaje_alarma.place(x=5, y=170)
        self.mensaje= Entry(self.lola.aux_windows[0], width = 30)
        self.mensaje.place(x=265, y=180)

        self.image = tk.PhotoImage(file=self.path + "lola_fn/imagenes/alarma/programar.png")
        Button(self.lola.aux_windows[0], image = self.image, command=self.callback, bg='#A9F5E1').place(x=250, y=250)

        self.imagen = tk.PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
        self.boton_salir = Button (self.lola.aux_windows[0], image = self.imagen, command=self.salir, bg='#A9F5E1').place(x=670, y=350)
        self.loop()

    def salir(self):
        self.lola.aux_windows[0].withdraw()
        self.lola.aux_windows[0].quit()

    def validateDate(self, a):
        try:
            datetime.datetime.strptime(a, '%H:%M')
            return True
        except ValueError:
            return False

    def callback(self):
        def run():
            Hora_alarma=self.hora.get()
            actual = 0
            Hora_alarmaLable= self.hora.get()
            self.msg = self.mensaje.get()

            if not self.validateDate(Hora_alarma):
                self.window = Tk()
                self.window.title('Formato de alarma') 
                self.window.geometry("800x480")
                self.window.configure(background='#A9F5E1')

                Label(self.window, text="Formato de hora incorrecto hh:ss" , font=('Arial', '20'), bg='#A9F5E1',fg='#FF0000').pack()
                self.window.update()
                time.sleep(4)
                self.window.destroy()


            else:
                self.lola.aux_windows[0].withdraw()
                self.lola.aux_windows[0].quit()
                while Hora_alarma != actual: #PROBAR CON UN IF
                    actual = datetime.datetime.now().time().strftime('%H:%M')
                    time.sleep(1)
                #cola.put((tkMessageBox.showinfo, ("Exito", "La alarma se ha encendido"), {}))
                self.alarma()

        t=threading.Thread(target=run)
        t.start()
        

    def loop(self):
        try:
            while True:
                funcion, args, kwargs  = cola.get_nowait()
                funcion(*args, **kwargs)
        except:
            pass

        self.lola.aux_windows[0].after(100, self.loop)

    def handler(self, event):
        self.lola.aux_windows[0].destroy()
    
    def run(self, actions):
        self.lola.aux_windows[0].mainloop()

    def alarma(self): 
      
        pygame.mixer.init()
        pygame.mixer.music.load(self.path + 'lola_fn/sonidos/alarma.mp3')
        pygame.mixer.music.play()
        time.sleep(1)
        self.ventana = Tk()
        self.ventana.title('Mensaje de alarma') 
        self.ventana.geometry("800x480")
        self.ventana.configure(background='#A9F5E1')

        Label(self.ventana, text=self.msg , font=('Arial', '20'), bg='#A9F5E1',fg='#FF0000').place(x=150, y=100)
        self.ventana.update()
        time.sleep(10)
        self.ventana.destroy()
        pygame.mixer.music.stop()

################################################################################################
##############################    CLASE PARA EL CODIGO QR      #################################
################################################################################################


class qr(object): 
    def __init__(self, lola):
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        self.escaner()

    def escaner(self):
        self.flag = False
        self.camara = cv2.VideoCapture(0)
        c = 0
        font = cv2.FONT_HERSHEY_SIMPLEX 
     
        while True:
            val, frame = self.camara.read()
            #frame = cv2.flip(frame, -1) #Comentar si la pantalla sale al reves
            if val:
                frame_gris = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                superficie = frame_gris.shape 
                escaner = zbar.Scanner()
                result = escaner.scan(frame_gris)
                cv2.imshow('Detector de qr', frame)

                for codigo_qr in result:
                    if c<1:
                        self.dat = codigo_qr.data[:]
                 
                        print self.dat
                        self.func()
                        c = c+1
                        break

                k = cv2.waitKey(10) & 0xFF

                if self.flag == True:
                    break
                #if cv2.getWindowProperty('Detector de qr',cv2.WND_PROP_VISIBLE) < 1:   #Para volver al menú con la cruz de la ventana     
                #    break
                if k == 27: #Tecla ESC
                    #cv2.destroyAllWindows()
                    break
        cv2.destroyAllWindows()

    def func(self):
        if self.dat == "TFGJGG001": #Cambiar color de la ventana para niños con autismo 
            self.flag = True

            self.lola.aux_windows[0].geometry("800x480")
            self.lola.aux_windows[0].configure(background='#FE642E')

            img = Image.open(self.path + "lola_fn/qr/qr_store/I_001.png") 
            o_size = img.size   #Tamaño original de la imagen
            f_size = (800, 230) #Tamaño del canvas donde se mostrará la imagen
            factor = min(float(f_size[1])/o_size[1], float(f_size[0])/o_size[0])
            width = int(o_size[0] * factor)
            height = int(o_size[1] * factor)
            rimg = img.resize((width,height), Image.ANTIALIAS)
            rimg = ImageTk.PhotoImage(rimg)
            imagen = tk.Label(self.lola.aux_windows[0], image=rimg, bg='#FE642E').pack()

            archivo = open("lola_fn/qr/qr_store/T_001.txt", 'r')
            mensaje = archivo.read()
            print mensaje 

            imagen_four = tk.PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
            boton = tk.Button(self.lola.aux_windows[0], image=imagen_four,bg='#FE642E', command=self.cerrar)
            boton.place(x=670, y=350)


            imagen_dos = tk.Label(self.lola.aux_windows[0], text=mensaje, bg='#FE642E', font=('Arial', 50)).pack()
            archivo.close()

            image_tres = tk.PhotoImage(file=self.path + "lola_fn/qr/qr_store/q001.png")   
            imagen_tres = tk.Label(self.lola.aux_windows[0], image=image_tres, bg='#FE642E').place(x=375, y=375)

            pygame.mixer.init()
            pygame.mixer.music.load(self.path + "lola_fn/qr/qr_store/A_001.mp3")
            pygame.mixer.music.play() #El 0 o nada del () es para reproducir una vez, si pongo -1 infinitamente
            self.lola.aux_windows[0].mainloop()
            

        if self.dat == "TFGJGG002": #Cambiar color de la ventana para niños con autismo 
            self.flag = True

            self.lola.aux_windows[0].geometry("800x480")
            self.lola.aux_windows[0].configure(background='#FE642E')

            img = Image.open(self.path + "lola_fn/qr/qr_store/I_002.png") 
            o_size = img.size   #Tamaño original de la imagen
            f_size = (800, 230) #Tamaño del canvas donde se mostrará la imagen
            factor = min(float(f_size[1])/o_size[1], float(f_size[0])/o_size[0])
            width = int(o_size[0] * factor)
            height = int(o_size[1] * factor)
            rimg = img.resize((width,height), Image.ANTIALIAS)
            rimg = ImageTk.PhotoImage(rimg)
            imagen = Label(self.lola.aux_windows[0], image=rimg, bg='#FE642E').pack()

            archivo = open("lola_fn/qr/qr_store/T_002.txt", 'r')
            mensaje = archivo.read()
            print mensaje 
            imagen_dos = Label(self.lola.aux_windows[0], text=mensaje, bg='#FE642E', font=('Arial', 50)).pack()
            archivo.close()

            image_tres = PhotoImage(file=self.path + "lola_fn/qr/qr_store/q002.png")   
            imagen_tres = Label(self.lola.aux_windows[0], image=image_tres, bg='#FE642E').place(x=375, y=375)

            pygame.mixer.init()
            pygame.mixer.music.load(self.path + "lola_fn/qr/qr_store/A_002.mp3")
            pygame.mixer.music.play() #El 0 o nada del () es para reproducir una vez, si pongo -1 infinitamente

            imagen_four = PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
            boton = Button(self.lola.aux_windows[0], image=imagen_four,bg='#FE642E', command=self.cerrar)
            boton.place(x=670, y=350)
            self.lola.aux_windows[0].mainloop()

        if self.dat == "TFGJGG003": #Cambiar color de la ventana para niños con autismo
            self.flag = True 
            self.lola.aux_windows[0].geometry("800x480")
            self.lola.aux_windows[0].configure(background='#FE642E')

            img = Image.open(self.path + "lola_fn/qr/qr_store/I_003.png") 
            o_size = img.size   #Tamaño original de la imagen
            f_size = (800, 230) #Tamaño del canvas donde se mostrará la imagen
            factor = min(float(f_size[1])/o_size[1], float(f_size[0])/o_size[0])
            width = int(o_size[0] * factor)
            height = int(o_size[1] * factor)
            rimg = img.resize((width,height), Image.ANTIALIAS)
            rimg = ImageTk.PhotoImage(rimg)
            imagen = Label(self.lola.aux_windows[0], image=rimg, bg='#FE642E').pack()

            archivo = open("lola_fn/qr/qr_store/T_003.txt", 'r')
            mensaje = archivo.read()
            print mensaje
            imagen_dos = Label(self.lola.aux_windows[0], text=mensaje, bg='#FE642E', font=('Arial', 50)).pack()
            archivo.close()

            image_tres = PhotoImage(file=self.path + "lola_fn/qr/qr_store/q003.png")   
            imagen_tres = Label(self.lola.aux_windows[0], image=image_tres, bg='#FE642E').place(x=375, y=375)

            pygame.mixer.init()
            pygame.mixer.music.load(self.path + "lola_fn/qr/qr_store/A_003.mp3")
            pygame.mixer.music.play()

            imagen_four = PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
            boton = Button(self.lola.aux_windows[0], image=imagen_four,bg='#FE642E', command=self.cerrar)
            boton.place(x=670, y=350)
            self.lola.aux_windows[0].mainloop()


        if self.dat == "TFGJGG004": #Cambiar color de la ventana para niños con autismo
            self.flag = True 

            self.lola.aux_windows[0].geometry("800x480")
            self.lola.aux_windows[0].configure(background='#FE642E')

            img = Image.open(self.path + "lola_fn/qr/qr_store/I_004.png") 
            o_size = img.size   #Tamaño original de la imagen
            f_size = (800, 230) #Tamaño del canvas donde se mostrará la imagen
            factor = min(float(f_size[1])/o_size[1], float(f_size[0])/o_size[0])
            width = int(o_size[0] * factor)
            height = int(o_size[1] * factor)
            rimg = img.resize((width,height), Image.ANTIALIAS)
            rimg = ImageTk.PhotoImage(rimg)
            imagen = Label(self.lola.aux_windows[0], image=rimg, bg='#FE642E').pack()

            archivo = open("lola_fn/qr/qr_store/T_004.txt", 'r')
            mensaje = archivo.read()
            print mensaje 
            imagen_dos = Label(self.lola.aux_windows[0], text=mensaje, bg='#FE642E', font=('Arial', 50)).pack()
            archivo.close()

            image_tres = PhotoImage(file=self.path + "lola_fn/qr/qr_store/q004.png")   
            imagen_tres = Label(self.lola.aux_windows[0], image=image_tres, bg='#FE642E').place(x=375, y=375)

            pygame.mixer.init()
            pygame.mixer.music.load(self.path + "lola_fn/qr/qr_store/A_004.mp3")
            pygame.mixer.music.play()

            imagen_four = PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
            boton = Button(self.lola.aux_windows[0], image=imagen_four,bg='#FE642E', command=self.cerrar)
            boton.place(x=670, y=350)
            self.lola.aux_windows[0].mainloop()

        if self.dat == "TFGJGG005": #Cambiar color de la ventana para niños con autismo 
            self.flag = True 
            self.lola.aux_windows[0].geometry("800x480")
            self.lola.aux_windows[0].configure(background='#FE642E')

            img = Image.open(self.path + "lola_fn/qr/qr_store/I_005.png") 
            o_size = img.size   #Tamaño original de la imagen
            f_size = (800, 230) #Tamaño del canvas donde se mostrará la imagen
            factor = min(float(f_size[1])/o_size[1], float(f_size[0])/o_size[0])
            width = int(o_size[0] * factor)
            height = int(o_size[1] * factor)
            rimg = img.resize((width,height), Image.ANTIALIAS)
            rimg = ImageTk.PhotoImage(rimg)
            imagen = Label(self.lola.aux_windows[0], image=rimg, bg='#FE642E').pack()

            archivo = open("lola_fn/qr/qr_store/T_005.txt", 'r')
            mensaje = archivo.read()
            print mensaje
            imagen_dos = Label(self.lola.aux_windows[0], text=mensaje, bg='#FE642E', font=('Arial', 50)).pack()
            archivo.close()

            image_tres = PhotoImage(file=self.path + "lola_fn/qr/qr_store/q005.png")   
            imagen_tres = Label(self.lola.aux_windows[0], image=image_tres, bg='#FE642E').place(x=375, y=375)

            pygame.mixer.init()
            pygame.mixer.music.load(self.path + "lola_fn/qr/qr_store/A_005.mp3")
            pygame.mixer.music.play()

            imagen_four = PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
            boton = Button(self.lola.aux_windows[0], image=imagen_four,bg='#FE642E', command=self.cerrar)
            boton.place(x=670, y=350)
            self.lola.aux_windows[0].mainloop()

        if self.dat == "TFGJGG006":
            try:
                email_from = 'padrinotecn@gmail.com'
                email_to  = 'jesus_gg94@hotmail.com'
                msg = 'Alarma'
 
                usuario = 'padrinotecn@gmail.com'
                password = 'padrinotecnologico'
 
                server = smtplib.SMTP('smtp.gmail.com:587')
                server.starttls()
                server.login(usuario,password)
                server.sendmail(email_from, email_to, msg)
                server.quit()
                print "Correo enviado correctamente"
                self.flag = True 
            except:
                cv2.destroyAllWindows()
                self.lola.aux_windows[0].destroy()

        if self.dat == "TFGJGG007": 
            try:
                email_from = 'padrinotecn@gmail.com'
                email_to  = 'jesus_gg94@hotmail.com'
                msg = 'Estoy bien'
 
                usuario = 'padrinotecn@gmail.com'
                password = 'padrinotecnologico'
 
                server = smtplib.SMTP('smtp.gmail.com:587')
                server.starttls()
                server.login(usuario,password)
                server.sendmail(email_from, email_to, msg)
                server.quit()
                print "Correo enviado correctamente"
                self.flag = True 
            except:
                cv2.destroyAllWindows()
                self.lola.aux_windows[0].destroy()

        if self.dat == "TFGJGG008": #Cambiar color de la ventana para niños con autismo 
            self.flag = True 
            self.lola.aux_windows[0].geometry("800x480")
            self.lola.aux_windows[0].configure(background='#FE642E')
            image_file = self.path + "lola_fn/qr/qr_store/I_008.png"

            # paint the screen
            self.image = tk.PhotoImage(file=image_file)
            self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="#FE642E")
            self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

            # Bind the screen clic to the "handler" method
            self.canvas.bind("<Button-1>",  self.handler)

            # prepare the screen
            self.canvas.pack()

            pygame.mixer.init()
            pygame.mixer.music.load(self.path + "lola_fn/qr/qr_store/A_008.ogg")
            pygame.mixer.music.play()
            # update the screen (what the uses sees)
            self.lola.aux_windows[0].mainloop()

        if self.dat == "TFGJGG009": #Cambiar color de la ventana para niños con autismo 
            self.flag = True 
            self.lola.aux_windows[0].geometry("800x480")
            self.lola.aux_windows[0].configure(background='#FE642E')

            self.image_file = Image.open(self.path + "lola_fn/qr/qr_store/I_009.png")
            # paint the screen
            self.image = ImageTk.PhotoImage(self.image_file)
            self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="#FE642E")
            self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

            # Bind the screen clic to the "handler" method
            self.canvas.bind("<Button-1>",  self.handler)

            # prepare the screen
            self.canvas.pack()


            #pygame.mixer.init()
            # Ya está inicializado al principio
            pygame.mixer.music.load(self.path + "lola_fn/qr/qr_store/A_009.ogg")
            pygame.mixer.music.play()
            # update the screen (what the uses sees)
            self.lola.aux_windows[0].mainloop()

        if self.dat == "TFGJGG010": 
            self.flag = True 
            self.lola.aux_windows[0].geometry("800x480")
            self.lola.aux_windows[0].configure(background='#FE642E')

            self.image_file = Image.open(self.path + "lola_fn/qr/qr_store/I_010.png")
            # paint the screen
            self.image = ImageTk.PhotoImage(self.image_file)
            self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="#FE642E")
            self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

            # Bind the screen clic to the "handler" method
            self.canvas.bind("<Button-1>",  self.handler)

            # prepare the screen
            self.canvas.pack()
            #pygame.mixer.init()
            # Ya está inicializado al principio
            pygame.mixer.music.load(self.path + "lola_fn/qr/qr_store/A_010.ogg")
            pygame.mixer.music.play()
            # update the screen (what the uses sees)
            self.lola.aux_windows[0].mainloop()
    def cerrar(self):
        pygame.mixer.music.stop()
        self.lola.aux_windows[0].withdraw()
        self.lola.aux_windows[0].quit()

    def handler(self, event):
        pygame.mixer.music.stop()
        self.lola.aux_windows[0].withdraw()
        self.lola.aux_windows[0].quit()


    def run(self, actions=None):
        self.lola.aux_windows[0].destroy()
 
class correo_auto: 
    def __init__(self, lola):
        self.lola = lola

    def accion(self):
        try:
            self.lola.aux_windows[0].withdraw()
            email_from = 'robot@tecnoidea.es'
            email_to  = 'j.gomezgarcia@edu.uah.es'
            asunto = 'Estado del usuario'
            msg = 'Alarma'
            usuario = 'robot@tecnoidea.es'
            password = 'Mail456rob$'

            headers = "From: %s\nTo: %s\nSubject:%s\n\n" %(email_from, email_to, asunto)
            body = str(headers + msg)

            server = smtplib.SMTP('mail.tecnoidea.es:26')
            #server = smtplib.SMTP('smtp.office365.com:587') #Para la UAH
            server.starttls()
            server.login(usuario,password)
            server.sendmail(email_from, email_to, body)
            server.quit()
            print "Correo enviado correctamente"

        except:
            self.lola.synthesizer.say("<speak>No se ha podido enviar el mensaje.</speak>")
            print "No se ha podido enviar el correo electronico"
            #time.sleep(5)
            self.lola.aux_windows[0].destroy()

    def accion2(self):
        try:
            self.lola.aux_windows[0].withdraw()
            email_from = 'robot@tecnoidea.es'
            email_to  = 'j.gomezgarcia@edu.uah.es'
            asunto = 'Estado del usuario'
            msg = 'Estoy bien'
            usuario = 'robot@tecnoidea.es'
            password = 'Mail456rob$'

            headers = "From: %s\nTo: %s\nSubject:%s\n\n" %(email_from, email_to, asunto)
            body = str(headers + msg)

            #server = smtplib.SMTP('smtp.office365.com:587') #Para la UAH
            server = smtplib.SMTP('mail.tecnoidea.es:26')
            server.starttls()
            server.login(usuario,password)
            server.sendmail(email_from, email_to, body)
            server.quit()
            print "Correo enviado correctamente"

        except:
            self.lola.synthesizer.say("<speak>No se ha podido enviar el mensaje.</speak>")
            print "No se ha podido enviar el correo electronico"
            self.lola.aux_windows[0].destroy()

    def run(self, actions=None):
        # comparamos con los nombres que están en pymenu.yaml
        if (actions[-1]=='alarma'):
            self.accion()
        elif (actions[-1]=='good'):
            self.accion2()

class puzle:
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola


    def func1(self):
        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        image_file = self.path + "lola_fn/imagenes/cuento/cuento.gif"

        # paint the screen
        self.image = tk.PhotoImage(file=image_file)
        self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="blue")
        self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

        # Bind the screen clic to the "handler" method
        self.canvas.bind("<Button-1>",  self.handler)

        # prepare the screen
        self.canvas.pack()

        # update the screen (what the uses sees)
        self.lola.aux_windows[0].update()

    def func2(self):
        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        image_file = self.path + "lola_fn/images/Correcto.gif"

        # paint the screen
        self.image = tk.PhotoImage(file=image_file)
        self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="blue")
        self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

        # Bind the screen clic to the "handler" method
        self.canvas.bind("<Button-1>",  self.handler)

        # prepare the screen
        self.canvas.pack()

        # update the screen (what the uses sees)
        self.lola.aux_windows[0].update()

    def func3(self):
        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        image_file = self.path + "lola_fn/images/Incorrecto.gif"

        # paint the screen
        self.image = tk.PhotoImage(file=image_file)
        self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="blue")
        self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

        # Bind the screen clic to the "handler" method
        self.canvas.bind("<Button-1>",  self.handler)

        # prepare the screen
        self.canvas.pack()

        # update the screen (what the uses sees)
        self.lola.aux_windows[0].update()

    def func4(self):
        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        image_file = self.path + "lola_fn/torre-eiffel.gif"

        # paint the screen
        self.image = tk.PhotoImage(file=image_file)
        self.canvas = tk.Canvas(self.lola.aux_windows[0], height=480, width=800, bg="blue")
        self.canvas.create_image(800 / 2, 480 / 2, image=self.image)

        # Bind the screen clic to the "handler" method
        self.canvas.bind("<Button-1>",  self.handler)

        # prepare the screen
        self.canvas.pack()

        # update the screen (what the uses sees)
        self.lola.aux_windows[0].update()

    def handler(self, event):
        self.lola.aux_windows[0].destroy()

    def run(self, actions):
        print (actions[0],actions[-1])
        if (actions[-1]=='imagen1'):
            self.func1()
        elif (actions[-1]=='imagen2'): 
            self.func2()
        elif (actions[-1]=='imagen3'): 
            self.func3()
        elif (actions[-1]=='imagen4'): 
            self.func4()

class captura:
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'

    def enviar(self):

        self.cuenta = 'robot@tecnoidea.es'
        self.password = 'Mail456rob$'
        self.to = 'jesus_gg94@hotmail.com'

        msg = MIMEMultipart()
        msg['To'] = 'jesus_gg94@hotmail.com'
        msg['From'] = 'robot@tecnoidea.es'
        msg['Subject'] = 'Correo con foto'
        msg.attach(MIMEText('Es una prueba'))
        fp = open("foto_enviada.png", 'rb')
        adjunto = MIMEImage(fp.read())
        adjunto.add_header("Content-Disposition","attachment",filename='captura-png')
        msg.attach(adjunto)

        self.server = smtplib.SMTP('mail.tecnoidea.es:26')
        self.server.starttls()
        self.server.login(self.cuenta,self.password)
        self.server.sendmail(self.cuenta, self.to, msg.as_string())
       	self.server.quit()

    def send_photo(self):
        #Numero de frame, mientras la camara se ajusta a los niveles de luz
        frame = 1
        self.lola.aux_windows[0].geometry("800x480")
        self.lola.aux_windows[0].configure(background='#F4FA58')
        Label(self.lola.aux_windows[0], text="Posa para la foto", font=('Arial', '50'), bg='#F4FA58',fg='#DF013A').place(x=175, y=150)
        self.lola.aux_windows[0].update()
        time.sleep(4)
        #iniciar camara
        camara = cv2.VideoCapture(0)
        self.lola.aux_windows[0].withdraw()

        def cap_image():
            # leer la captura
            val, image = camara.read()
            #image = cv2.flip(image, -1) #Comentar si la pantalla hace la foto al revés
            return image
        for i in xrange(frame):
            temp = cap_image()
        print("Foto")

        # entregar imagen leida anteriormente
        camara_capture = cap_image()
        file = "foto_enviada.png"
        # Guardar la imagen con opencv que fue leida por PIL
        cv2.imwrite(file, camara_capture)
        self.ventana = Toplevel()
        self.ventana.geometry("800x480")
        self.ventana.title("Foto enviada")
        self.ventana.configure(background='white')

        img = Image.open("foto_enviada.png") 

        rimg = ImageTk.PhotoImage(img)
        imagen = tk.Label(self.ventana, image=rimg, bg='#FE642E').pack()
        self.ventana.update()

        time.sleep(5)
        self.ventana.destroy()

        self.enviar()

    def photo(self):
        #Numero de frame, mientras la camara se ajusta a los niveles de luz
        frame = 1
        #iniciar camara
        camara = cv2.VideoCapture(0)
        self.lola.aux_windows[0].withdraw()

        def cap_image():
            # leer la captura
            val, image = camara.read()
            #image = cv2.flip(image, -1) #Comentar si la pantalla hace la foto al revés
            return image
        for i in xrange(frame):
            temp = cap_image()
        print("Foto")

        # entregar imagen leida anteriormente
        camara_capture = cap_image()
        file = "foto.png"
        # Guardar la imagen con opencv que fue leida por PIL
        cv2.imwrite(file, camara_capture)

        self.ventana = Toplevel()
        self.ventana.geometry("800x480")
        self.ventana.title("Captura de foto")
        self.ventana.configure(background='white')

        img = Image.open("foto.png") 

        rimg = ImageTk.PhotoImage(img)
        imagen = tk.Label(self.ventana, image=rimg, bg='#FE642E').pack()
        self.ventana.update()

        time.sleep(5)
        self.ventana.destroy()

    def run(self, actions=None):
        # comparamos con los nombres que están en pymenu.yaml
        if (actions[-1]=='send_photo'):
            self.send_photo()
        elif (actions[-1]=='photo'):
            self.photo()

class calendario:
    def __init__(self, lola):
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'

    def calendario(self):
        self.lola.aux_windows[0].title("CALENDARIO")
        self.lola.aux_windows[0].geometry("800x480")
        self.lola.aux_windows[0].configure(background='#F4FA58')

        Label(self.lola.aux_windows[0], text="Calendario", font=('Arial', '50', 'bold italic'), bg='#F4FA58',fg='#DF013A').place(x=215, y=1)
        self.image = PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
        Button(self.lola.aux_windows[0], image=self.image, command=self.quit ,bg='#F4FA58',fg='#DF013A').place(x=675, y=325)

        width, height = 650, 350
        self.calendar = ShockCalendar.Calendar(self.lola.aux_windows[0], width, height)
        self.calendar.place(x=5, y=100, width=width, height=height)

    def agenda(self):
        self.lola.aux_windows[0].title("Agenda diaria")
        self.lola.aux_windows[0].geometry("800x480")
        self.lola.aux_windows[0].configure(background='#F4FA58')

        Label(self.lola.aux_windows[0], text='Actividades diarias',fg='blue', bg='#F4FA58', font=('Arial', 50)).place(x=135, y=5)

        archivo = open("lola_fn/agenda.txt", 'r')
        agenda = archivo.read()
        print agenda 
        Label(self.lola.aux_windows[0], text=agenda, bg='#F4FA58', fg='blue', font=('Arial', 25), justify = tk.LEFT).place(x=5, y=100)
        archivo.close()

        self.image = PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
        Button(self.lola.aux_windows[0], image=self.image, command=self.quit ,bg='#F4FA58',fg='#DF013A').place(x=675, y=325)   

    def quit(self):
        self.lola.aux_windows[0].withdraw()
        self.lola.aux_windows[0].quit()

    def run(self, actions=None):
        if actions[-1] == 'calendario':
            self.calendario()
            self.lola.aux_windows[0].mainloop()
        elif actions[-1] == 'agenda':
            self.agenda()
            self.lola.aux_windows[0].mainloop()

class movimiento:
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        self.arduino = serial.Serial('/dev/ttyAMA0', 9600) #Conexion serie USB  
        #ser=serial.Serial('/dev/ttyAMA0',9600,timeout=3)
        #ser=serial.serial('/dev/cu.wchusbserial1410',9600,timeout=3)
        print "Esperando inicializacion"
        time.sleep(4)

    def move(self):
        #Inicializacion previa conexion serial
        self.arduino.write('40500')    # Se supone que la cadena para avanzar es '4' mas distancia en milimetros
                              # Avanza a la velocidad por defecto #El 4 comando avanzar/0500 distancia que avanza
        a = self.arduino.write(';')
        print a

        time.sleep(8)
        self.arduino.write('9180100')  # '9XXXVVV'  XXX Grados de giro  VVV: Velocidad
        b = self.arduino.write(';')
        print b
        time.sleep(8)

         #Inicializacion previa conexion serial
        self.arduino.write('40500')    # Se supone que la cadena para avanzar es '4' mas distancia en milimetros
                              # Avanza a la velocidad por defecto #El 4 comando avanzar/0500 distancia que avanza
        c = self.arduino.write(';')
        print c

        time.sleep(8)
        self.arduino.write('9180100')  # '9XXXVVV'  XXX Grados de giro  VVV: Velocidad
        d = self.arduino.write(';')
        print d
        time.sleep(4)


    def run(self, actions=None):
        self.move()
        self.lola.aux_windows[0].withdraw()



# Timeout thread
class Watchdog(threading.Thread):
    def __init__(self):
        super(Watchdog, self).__init__()
        self.event = threading.Event()
        self.terminated = False
        self.start()
        self.timestamp = time.time()

    def run(self):
        timedOut = True
        # This method runs in a separate thread
        while not self.terminated:
            # Wait for a network event to be flagged for up to one second
            if timedOut:
                if self.event.wait(1):
                    # Connection
                    print 'Reconnected...'
                    timedOut = False
                    self.event.clear()
            else:
                if self.event.wait(1):
                    self.event.clear()
                else:
                    # Timed out
                    print 'Timed out...'
                    timedOut = True

# Image stream processing thread
class StreamProcessor(threading.Thread):
    def __init__(self):
        super(StreamProcessor, self).__init__()
        self.stream = picamera.array.PiRGBArray(camera)
        self.event = threading.Event()
        self.terminated = False
        self.start()
        self.begin = 0

    def run(self):
        global lastFrame
        global lockFrame
        # This method runs in a separate thread
        while not self.terminated:
            # Wait for an image to be written to the stream
            if self.event.wait(1):
                try:
                    # Read the image and save globally
                    self.stream.seek(0)
                    flippedArray = cv2.flip(self.stream.array, -1) # Flips X and Y
                    retval, thisFrame = cv2.imencode('.jpg', flippedArray)
                    del flippedArray
                    lockFrame.acquire()
                    lastFrame = thisFrame
                    lockFrame.release()
                finally:
                    # Reset the stream and event
                    self.stream.seek(0)
                    self.stream.truncate()
                    self.event.clear()

# Image capture thread
class ImageCapture(threading.Thread):
    def __init__(self):
        super(ImageCapture, self).__init__()
        self.start()

    def run(self):
        global camera
        global processor
        print 'Start the stream using the video port'
        camera.capture_sequence(self.TriggerStream(), format='bgr', use_video_port=True)
        print 'Terminating camera processing...'
        self.processor.terminated = True
        self.processor.join()
        print 'Processing terminated.'

    # Stream delegation loop
    def TriggerStream(self):
        global running
        while running:
            if processor.event.is_set():
                time.sleep(0.01)
            else:
                yield processor.stream
                self.processor.event.set()


class WebServer(SocketServer.BaseRequestHandler):
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        self.lola.aux_windows[0].withdraw()


    def callback(self):
        def run():
            result = commands.getoutput('/usr/bin/python VisorWeb.py')

        t=threading.Thread(target=run)
        t.start()


    def run(self, actions=None):
        self.callback()

class ProxQR():
    def __init__(self, lola):
        # copy the lola object to self.lola, so it can be used later
        self.lola = lola

        # get the program's PATH so we can find the picture and sound files
        self.path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/' 
        

    def exploracion(self):
        cam=cv2.VideoCapture(0)
        #Resolución Width
        cam.set(3,1024)
        #Resolución Height
        cam.set(4,768)

        res, img=cam.read()
        tam=img.shape[0:2]
        f=0.79*tam[1]
        K=np.matrix([
        [f, 0, tam[1]/2],
        [0, f, tam[0]/2],
        [0, 0,        1]], np.float32)

	    #Por el momento solo está implementado el camino horizontal en el mapa. La referencia de desviación por tanto es la coordenada Y
        global X,Y,ANG, MAPSCALE, anguloCamino, i, ser, Optimal_path, yRoom 
	    #La variable global i es la cantidad de nodos recorridos
        ANG = 90
        anguloCamino = ANG
        nodosRecorrer = 0
        #indice variable auxiliar para ir marcando los quiebros
        indice = 0
        quiebros = []
        direcciones = []
        nnn=0
        while(nnn<1):
            print("===============")
            print("===============")
            nnn=nnn+1
			#Leemos posicion inicial
            res, img=cam.read()
            if not res: break
            try:
                frame_gris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                escaner = zbar.Scanner()
                result = escaner.scan(frame_gris)
                time.sleep(3)
                print("Codigo:")
                for codigo_qr in result:
                    self.dat = codigo_qr.data[:]
                    print self.dat
                print("Angulo y posicion: ")
                r, p=CalculoPose.CalculoPoseQR(img, puntos, K)
                plAux=np.copy(plano)
                R, _=cv2.Rodrigues(r)
                R=np.matrix(R)
                R2=R.T
                p2=R2*p
                print(p2[0,0], r[1,0], p2[2,0], np.linalg.norm(p))
                if -0.2<r[1,0]<0:
                    self.lola.aux_windows[0].geometry("800x480")
                    self.lola.aux_windows[0].configure(background='#9FF781')
                    Label(self.lola.aux_windows[0], text="Codigo:", fg='#FF0000',bg='#9FF781',font=('Arial', '20')).place(x=475, y=30)
                    Label(self.lola.aux_windows[0], text=self.dat , fg='#FF0000', bg='#9FF781',font=('Arial', '20')).place(x=575, y=30)

                    Label(self.lola.aux_windows[0], text="P1:", fg='#FF0000',bg='#9FF781',font=('Arial', '20')).place(x=550, y=80)
                    Label(self.lola.aux_windows[0], text= "%.2f" % p2[0,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=80)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=665, y=80)

                    Label(self.lola.aux_windows[0], text="P2:", fg='#FF0000', bg='#9FF781',font=('Arial', '20')).place(x=550, y=130)
                    Label(self.lola.aux_windows[0], text="%.2f" % r[1,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=130)
                    Label(self.lola.aux_windows[0], text= "rad", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=665, y=130)

                    Label(self.lola.aux_windows[0], text="P3:", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=550, y=180)
                    Label(self.lola.aux_windows[0], text="%.2f" % p2[2,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=180)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=675, y=180)

                    Label(self.lola.aux_windows[0], text="P4:", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=550, y=230)
                    Label(self.lola.aux_windows[0], text="%.2f" % np.linalg.norm(p), fg='#FF0000', bg='#9FF781', font=('Arial', '20')).place(x=600, y=230)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=675, y=230)

                    self.image = PhotoImage(file=self.path + "lola_fn/imagenes/c1.png")   
                    Label(self.lola.aux_windows[0], image=self.image, bg='#A9F5E1',anchor="center").place(x=15, y=15)

                    imagen = tk.PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
                    boton = tk.Button(self.lola.aux_windows[0], image=imagen,bg='#9FF781', command=self.cerrar)
                    boton.place(x=670, y=350)
                    self.lola.aux_windows[0].mainloop()
                elif 0<r[1,0]<0.1:
                    self.lola.aux_windows[0].geometry("800x480")
                    self.lola.aux_windows[0].configure(background='#9FF781')
                    Label(self.lola.aux_windows[0], text="Codigo:", fg='#FF0000',bg='#9FF781',font=('Arial', '20')).place(x=475, y=30)
                    Label(self.lola.aux_windows[0], text=self.dat , fg='#FF0000', bg='#9FF781',font=('Arial', '20')).place(x=575, y=30)

                    Label(self.lola.aux_windows[0], text="P1:", fg='#FF0000',bg='#9FF781',font=('Arial', '20')).place(x=550, y=80)
                    Label(self.lola.aux_windows[0], text= "%.2f" % p2[0,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=80)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=665, y=80)

                    Label(self.lola.aux_windows[0], text="P2:", fg='#FF0000', bg='#9FF781',font=('Arial', '20')).place(x=550, y=130)
                    Label(self.lola.aux_windows[0], text="%.2f" % r[1,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=130)
                    Label(self.lola.aux_windows[0], text= "rad", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=665, y=130)

                    Label(self.lola.aux_windows[0], text="P3:", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=550, y=180)
                    Label(self.lola.aux_windows[0], text="%.2f" % p2[2,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=180)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=675, y=180)

                    Label(self.lola.aux_windows[0], text="P4:", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=550, y=230)
                    Label(self.lola.aux_windows[0], text="%.2f" % np.linalg.norm(p), fg='#FF0000', bg='#9FF781', font=('Arial', '20')).place(x=600, y=230)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=675, y=230)

                    self.image = PhotoImage(file=self.path + "lola_fn/imagenes/b1.png")   
                    Label(self.lola.aux_windows[0], image=self.image, bg='#A9F5E1',anchor="center").place(x=15, y=15)

                    imagen = tk.PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
                    boton = tk.Button(self.lola.aux_windows[0], image=imagen,bg='#9FF781', command=self.cerrar)
                    boton.place(x=670, y=350)
                    self.lola.aux_windows[0].mainloop()
                elif r[1,0]>0.1:
                    self.lola.aux_windows[0].geometry("800x480")
                    self.lola.aux_windows[0].configure(background='#9FF781')
                    Label(self.lola.aux_windows[0], text="Codigo:", fg='#FF0000',bg='#9FF781',font=('Arial', '20')).place(x=475, y=30)
                    Label(self.lola.aux_windows[0], text=self.dat , fg='#FF0000', bg='#9FF781',font=('Arial', '20')).place(x=575, y=30)

                    Label(self.lola.aux_windows[0], text="P1:", fg='#FF0000',bg='#9FF781',font=('Arial', '20')).place(x=550, y=80)
                    Label(self.lola.aux_windows[0], text= "%.2f" % p2[0,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=80)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=665, y=80)

                    Label(self.lola.aux_windows[0], text="P2:", fg='#FF0000', bg='#9FF781',font=('Arial', '20')).place(x=550, y=130)
                    Label(self.lola.aux_windows[0], text="%.2f" % r[1,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=130)
                    Label(self.lola.aux_windows[0], text= "rad", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=665, y=130)

                    Label(self.lola.aux_windows[0], text="P3:", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=550, y=180)
                    Label(self.lola.aux_windows[0], text="%.2f" % p2[2,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=180)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=675, y=180)

                    Label(self.lola.aux_windows[0], text="P4:", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=550, y=230)
                    Label(self.lola.aux_windows[0], text="%.2f" % np.linalg.norm(p), fg='#FF0000', bg='#9FF781', font=('Arial', '20')).place(x=600, y=230)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=675, y=230)

                    self.image = PhotoImage(file=self.path + "lola_fn/imagenes/a1.png")   
                    Label(self.lola.aux_windows[0], image=self.image, bg='#A9F5E1',anchor="center").place(x=15, y=15)

                    imagen = tk.PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
                    boton = tk.Button(self.lola.aux_windows[0], image=imagen,bg='#9FF781', command=self.cerrar)
                    boton.place(x=670, y=350)
                    self.lola.aux_windows[0].mainloop()

                elif r[1,0]<-0.2:
                    self.lola.aux_windows[0].geometry("800x480")
                    self.lola.aux_windows[0].configure(background='#9FF781')
                    Label(self.lola.aux_windows[0], text="Codigo:", fg='#FF0000',bg='#9FF781',font=('Arial', '20')).place(x=475, y=30)
                    Label(self.lola.aux_windows[0], text=self.dat , fg='#FF0000', bg='#9FF781',font=('Arial', '20')).place(x=575, y=30)

                    Label(self.lola.aux_windows[0], text="P1:", fg='#FF0000',bg='#9FF781',font=('Arial', '20')).place(x=550, y=80)
                    Label(self.lola.aux_windows[0], text= "%.2f" % p2[0,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=80)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=665, y=80)

                    Label(self.lola.aux_windows[0], text="P2:", fg='#FF0000', bg='#9FF781',font=('Arial', '20')).place(x=550, y=130)
                    Label(self.lola.aux_windows[0], text="%.2f" % r[1,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=130)
                    Label(self.lola.aux_windows[0], text= "rad", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=665, y=130)

                    Label(self.lola.aux_windows[0], text="P3:", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=550, y=180)
                    Label(self.lola.aux_windows[0], text="%.2f" % p2[2,0], fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=600, y=180)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=675, y=180)

                    Label(self.lola.aux_windows[0], text="P4:", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=550, y=230)
                    Label(self.lola.aux_windows[0], text="%.2f" % np.linalg.norm(p), fg='#FF0000', bg='#9FF781', font=('Arial', '20')).place(x=600, y=230)
                    Label(self.lola.aux_windows[0], text= "cm", fg='#FF0000',bg='#9FF781', font=('Arial', '20')).place(x=675, y=230)

                    self.image = PhotoImage(file=self.path + "lola_fn/imagenes/d1.png")   
                    Label(self.lola.aux_windows[0], image=self.image, bg='#A9F5E1',anchor="center").place(x=15, y=15)

                    imagen = tk.PhotoImage(file=self.path + "lola_fn/qr/imagenes/volver.png")
                    boton = tk.Button(self.lola.aux_windows[0], image=imagen,bg='#9FF781', command=self.cerrar)
                    boton.place(x=670, y=350)
                    self.lola.aux_windows[0].mainloop()

            except ValueError:
                p2=np.matrix([[0],[0],[MAPSCALE*5/10]])
                r=np.matrix([[0],[0],[0]])
                pass

    def cerrar(self):
        self.lola.aux_windows[0].withdraw()
        self.lola.aux_windows[0].quit()

    def run(self, actions=None):
        self.exploracion()
        self.lola.aux_windows[0].withdraw()
