import serial
import time
import os
import math

#variable para correcion angular
correcion=0

def der(ser,angInicio,angGiro,x0,y0):
	print("Las coordenadas antes de girar a derecha son \n X = {} Y = {} ANG= {}".format(x0,y0,angInicio))
	time.sleep(1)
	#variables inicio posicion
	theta=math.radians(angInicio)
	#print("Angulo inicial en radianes = {}".format(ang))
	#variables calculo movimiento
	#Avance lineal por pulso del encoder
	cm = 0.2094
	#Nuestro diferencial de movimiento lo asociamos a delta numero de pulsos del encoder
	Delta = 15
	#Longitud del eje entre ruedas (mm)
	L = 190
	x = x0
	y = y0
	ValorD_delta = 0
	ValorD=0
	ValorI=0
	sR,sC,sL = 0,0,0
	theta_delta = 0
	derAnt=0
	izqAnt=0
	pulsosDER, pulsosIZQ = 0,0
	distanciaDER = 0
	auxDER,auxIZQ=0,0
	#variable para verificar que a parado
	iguales=0
	#comenzamos movimiento
	#ser.write('<'.encode('ascii'))
	#valor=ser.readline().decode('ascii')
	#print("Valor primero = {}".format(valor))
	#while valor[0:4]!=')0*0':       #condicion para evitar leer basura
		#ser.write(';'.encode('ascii'))
		#valor=ser.readline().decode('ascii')
	#print("Angulo a girar = {}".format(angGiro)) 
	if angGiro<=9:
		ser.write(('900'+str(angGiro)+'180').encode('ascii'))
	elif  angGiro<=99:
		ser.write(('90'+str(angGiro)+'180').encode('ascii'))
	else:
		ser.write(('9'+str(angGiro)+'180').encode('ascii'))	

	ser.write(';'.encode('ascii'))
	valor=ser.readline().decode('ascii')
	#print("VALOR = {}".format(valor))

		
	
	while (iguales<10):
		ser.write(';'.encode('ascii'))
		valor=ser.readline().decode('ascii')
		#print("valor en mitad de giro = {}".format(valor))
		if valor[0]!=')':       #condicion para evitar leer basura
			continue
		#Deacuerdo al formato recibido, los valores se separan por el *
		ValorII,ValorDD=valor[1:-2].split('*')
		#ValorI_anterior y ValorD_anterior son los pulsos de los encoder. Fijamos la distancia delta
		ValorI=int(ValorII)
		#print("ValorI = {}".format(ValorI))
		ValorD=int(ValorDD)
		
		
		if ValorD>ValorD_delta:
			auxDER = ValorD - pulsosDER
			auxIZQ = ValorI - pulsosIZQ

			pulsosDER = ValorD
			pulsosIZQ = ValorI
		
			sL = cm*auxIZQ
			sR = cm*auxDER
			#valor absoluto para no diferenciar si la desviación fue a izquierda o derecha
			theta_delta = (sR-sL)/L
			sC = (sR+sL)/2
			
			#Actualizamos las variables de la nueva posicion
			theta = theta - theta_delta
			x = x - sC*math.cos(theta)
			y = y + sC*math.sin(theta)

			ValorD_delta = ValorD + Delta
			ValorI_delta = ValorI + Delta
			


		if derAnt==ValorD:
			#print("IGUALES")
			iguales+=1
		else:
			iguales=0
			derAnt=ValorD
			izqAnt=ValorI
			


	#Cuando se cuenten unas 20 veces que las ruedas ya no se mueven, el giro ha finalizado
	ser.write('?'.encode('ascii'))
	time.sleep(0.5)
    #Si el giro a derecha es sobre rueda izquierda, el giro es como un inverso del giro a izquierda
	return (x,y,(math.degrees(theta)),ValorI,ValorD)

def izq(ser,angInicio,angGiro,x0,y0):
	#print("Funcion girar izquierda comienza")
	print("Las coordenadas antes de girar a izquierda son \n X = {} Y = {} ANG= {}".format(x0,y0,angInicio))
	#variables inicio posicion
	theta=math.radians(angInicio)
	#print("Angulo inicial en radianes = {}".format(ang))
	#variables calculo movimiento
	#Avance lineal por pulso del encoder
	cm = 0.2094
	#Nuestro diferencial de movimiento lo asociamos a delta numero de pulsos del encoder
	Delta = 15
	#Longitud del eje entre ruedas (mm)
	L = 190
	x = x0
	y = y0
	ValorD_delta = 0
	ValorD=0
	ValorI=0
	sR,sC,sL = 0,0,0
	theta_delta = 0
	derAnt=0
	izqAnt=0
	pulsosDER, pulsosIZQ = 0,0
	distanciaDER = 0
	auxDER,auxIZQ=0,0
	#variable para verificar que a parado
	iguales=0
	#comenzamos movimiento
	#ser.write('<'.encode('ascii'))
	#valor=ser.readline().decode('ascii')
	#while valor[0:4]!=')0*0':       #condicion para evitar leer basura
		#ser.write(';'.encode('ascii'))
		#valor=ser.readline().decode('ascii')
		#print("VALOR = {}".format(valor))
	#print("Angulo a girar = {}".format(angGiro))        
	if angGiro<=9:
		ser.write(('800'+str(angGiro)+'180').encode('ascii'))
	elif  angGiro<=99:
		ser.write(('80'+str(angGiro)+'180').encode('ascii'))
	else:
		ser.write(('8'+str(angGiro)+'180').encode('ascii'))

	ser.write(';'.encode('ascii'))
	valor=ser.readline().decode('ascii')
	#print("VALOR = {}".format(valor))

	#print("Valor primero = {}".format(valor))
		
	#time.sleep(1)
	while (iguales<10):
		ser.write(';'.encode('ascii'))
		valor=ser.readline().decode('ascii')
		#print("valor en mitad de giro = {}".format(valor))
		if valor[0]!=')':       #condicion para evitar leer basura
			continue
		#Deacuerdo al formato recibido, los valores se separan por el *
		ValorII,ValorDD=valor[1:-2].split('*')
		#ValorI_anterior y ValorD_anterior son los pulsos de los encoder. Fijamos la distancia delta
		ValorI=int(ValorII)
		ValorI_delta = ValorI + Delta
		#print("ValorI = {}".format(ValorI))
		ValorD=int(ValorDD)
		
		
		if ValorD>ValorD_delta:
			auxDER = ValorD - pulsosDER
			auxIZQ = ValorI - pulsosIZQ

			pulsosDER = ValorD
			pulsosIZQ = ValorI
		
			sL = cm*auxIZQ
			sR = cm*auxDER
			#valor absoluto para no diferenciar si la desviación fue a izquierda o derecha
			theta_delta = (sR-sL)/L
			sC = (sR+sL)/2
			
			#Actualizamos las variables de la nueva posicion
			theta = theta + theta_delta
			x = x + sC*math.cos(theta)
			y = y - sC*math.sin(theta)

			ValorD_delta = ValorD + Delta
			ValorI_delta = ValorI + Delta


		if derAnt==ValorD:
			#print("IGUALES")
			iguales+=1
		else:
			iguales=0
			derAnt=ValorD
			izqAnt=ValorI
			


	#print("Funcion girar izquierda finalizada")
	ser.write('?'.encode('ascii'))
	time.sleep(0.5)

	return (x,y,math.degrees(theta),ValorI,ValorD)

def main():
	#La variable global i es la longitud del array Optimal_path
	global X,Y,ANG, MAPSCALE, anguloCamino, i, ser, Optimal_path
	
	try:
		#Inicializacion previa conexion serial
		ser=serial.Serial('/dev/ttyAMA0',9600,timeout=3)
		#ser=serial.serial('/dev/cu.wchusbserial1410',9600,timeout=3)
		print ("Conectado")
		time.sleep(5)
		ser.write('<'.encode('ascii'))
		time.sleep(1)
		#Fin inicializacion
		X = 10400
		Y = 1300
		ANG = 90
		print("Las coordenadas iniciales son X={} Y={} ANG={}".format(X,Y,ANG))

		X,Y,ANG,ValorI,ValorD = izq(ser,ANG,90,X,Y)
		print("Las nuevas coordenadas son X = {}  Y = {}  ANG = {}".format(X,Y,ANG))
		input("Presione Intro para continuar")
		X,Y,ANG,ValorI,ValorD = der(ser,ANG,90,X,Y)
		print("Las nuevas coordenadas son X= {}  Y = {}  ANG = {}".format(X,Y,ANG))
		   
	except Exception as inst:
		#Informe error
		print (inst)
		#Fin informorme
	finally:
		#Cerrado y visualizacion
		ser.write('?'.encode('ascii'))
		time.sleep(0.5)
		ser.close()
		#Q.close()
			


if __name__ == '__main__':
	main()
