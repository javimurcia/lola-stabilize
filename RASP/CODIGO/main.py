#!/usr/bin/python2
# -*- coding: utf-8 -*-

# Custom TTS module, using polly and storing the audio for offline use
from polly_tts import *

# Touch menu, metro style
from pimenu import *

# Custom idle animation
#from cara import *

# Graphical interface
import Tkinter as Tk

# threading library
from threading import Thread


import time as t


class lola_ui():
    fullscreen = False
    timeout = 5000
    tts_online = True

    # Create the root window
    root = Tk.Tk()  # Create the root window. If this window is closed, the program stops
    Tk.Label(root, text="this is the root window").pack()  # root window title
    root.geometry("800x480")  # root window size, equals to raspi display resolution
    root.lower()
    if fullscreen:
        root.wm_attributes('-fullscreen', True)  # make the window fullscreen

    # Touch menu window
    window = Tk.Toplevel()  # create a new window, dependant on root
    window.geometry("800x480")  # Thats the raspi display resolution
    window.wm_title('PiMenu')  # Screen title, normally not shown
    if fullscreen:
        window.wm_attributes('-fullscreen', True)  # make the window fullscreen
    #window.lower()
    timeout_after_instance = None

    # Aditional Windows
    aux_windows = []
    aux_windows.append(None) # make sure th first element exists

    # Speech interface
    synthesizer = polly.VoiceSynthesizer(1.0, TextType='ssml', tts_online=tts_online)

    def start_timeout(self):
        self.timeout_after_instance = self.window.after(self.timeout, self.__timeout_handler__, self)

    def stop_timeout(self):
        if self.timeout_after_instance is not None:
            self.window.after_cancel(self.timeout_after_instance)
            self.timeout_after_instance = None
        else:
            print("No timeout_after_instance job")

    def __timeout_handler__(self):
        self.root.lift()
        t.sleep(0.2)
        self.window.lower()



def main():
    lola = lola_ui

    # Create a TTS object for speech synthesis
    lola.synthesizer.say("<speak>Hola, me llamo Lola. Toca la pantalla para comenzar</speak>")

    #TreadCara = Thread(target=cara.cara, name="animacion", args=(lola,))
    #TreadCara.start()

    TreadPimenu = Thread(target=pimenu.PiMenu, name="animacion", args=(lola,))
    TreadPimenu.start()

    #window.after(5000, window.iconify) # this is for testing purposes, after 5s close the menu screen.

    lola.root.mainloop()


if __name__ == "__main__":
    main()

'''

root = Tk()
root.geometry("800x480")
root.wm_title('PiMenu')
#root.wm_attributes('-fullscreen', True)
main = Window(root)
pimenu.PiMenu(root)
root.after(30000, root.destroy)
root.mainloop()
'''


